﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MarketingAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //enable CORS
            config.EnableCors();

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}"
                //defaults: new { zip = RouteParameter.Optional, producerID = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
              name: "VehicleApi",
              routeTemplate: "api/{controller}/{action}"
                //defaults: new { zip = RouteParameter.Optional, producerID = RouteParameter.Optional }
          );
        }
    }
}
