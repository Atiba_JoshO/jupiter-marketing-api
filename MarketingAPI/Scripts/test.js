﻿//var service = "http://consumerdirect.jupiterautoins.net/api";
var service = "http://localhost:3547/api";

var Payload = function () {
    this.Vehicles = [];
    this.Drivers = [];

    this.MainLocationZip = "";
    this.MainLocationCityState = "";
    this.ProducerId = 0;

    this.IsCurrentlyInsured = false;
    this.WhenIsCoverageNeeded = "";
    this.CoverageLimits = null;

    this.EmailAddress = "";
    this.PhoneNumber = "";
    this.StreetAddress = "";
    this.AptNumber = "";
    this.ZipCode = "";
    this.City = "";
    
    this.StateId = "";
    this.PolicyType = "";
    this.PolicyTerm = "";
    this.RatingVersionId = "";
    this.CompanyId = "";
    this.PaymentPlanId = "";
    this.DepositPercent = "";
    this.InstallmentCount = "";
    this.EffectiveDate = "";
    this.CreditScoreEntered = "";
    this.BodilyInjury = "";
    this.PropertyDamage = "";
    this.UninsuredBodilyInjury = "";
    this.UninsuredPropertyDamage = "";
    this.Comprehensive = "";
    this.Collision = "";
    this.AccidentalDeath = "";
    this.TowingAndLabor = "";
    this.RentalReimbursement = "";
    this.PersonalInjury = "";
    this.PersonalInjuryEachPerson = "";
    this.action = "";
    this.PolicyNumber = "";
    this.PolicyId = "";
    };

var Driver = function () {
    this.FirstName = "";
    this.LastName = "";
    this.MiddleName = "";
    this.DOB = "";
    this.Gender = "";
    this.IsMarried = false;
    this.IsViolations = false;
    this.Excluded = "0";
    this.SDIPPoints = "0";
};

var Vehicle = function () {
    this.Year = "";
    this.Make = "";
    this.Model = "";
    this.Style = "";
    this.IsPaid = true;
    this.IsFullCoverage = true;
    this.Comprehensive = "";
    this.Collision = "";
    this.Liability = "";
    this.VIN = "XXXXXXXXXXXXXXXXX";
    this.Usage = "";
    this.VINState = "";
    this.ValidVIN = "N";
};

var userInfo = new Payload();

var selectedDriver = null;
var selectedVehicle = null;

function setParams() {
    selectedVehicle = new Vehicle();

    selectedVehicle.Year = "1995";
    selectedVehicle.Make = "Eagle";
    selectedVehicle.Model = "Talon";
    selectedVehicle.Style = "3dr Hatchback ESi FWD";
    selectedVehicle.IsPaid = 1;
    selectedVehicle.IsFullCoverage = 1;
    selectedVehicle.Comprehensive = "274";
    selectedVehicle.Collision = "141";
    selectedVehicle.Liability = "158";
    selectedVehicle.VIN = "XXXXXXXXXXXXXXXXX";
    selectedVehicle.Usage = "W";
    selectedVehicle.VINState = "TN";
    selectedVehicle.ValidVIN = "N";  

    userInfo.Vehicles.push(selectedVehicle);

    Driver1 = new Driver();
    Driver2 = new Driver();

    Driver1.FirstName = "Josh";
    Driver1.LastName = "Owens";
    Driver1.MiddleName = "B";
    Driver1.DOB = "5-21-1977";
    Driver1.Gender = "1";
    Driver1.IsMarried = "1";
    Driver1.IsViolations = "0";
    Driver1.Excluded = "0";
    Driver1.SDIPPoints = "0";

    userInfo.Drivers.push(Driver1);

    Driver2.FirstName = "Cassie";
    Driver2.LastName = "Owens";
    Driver2.MiddleName = "B";
    Driver2.DOB = "5-21-1977";
    Driver2.Gender = "1";
    Driver2.IsMarried = "1";
    Driver2.IsViolations = "0";
    Driver2.Excluded = "0";
    Driver2.SDIPPoints = "0";

    userInfo.Drivers.push(Driver2);
    
    userInfo.MainLocationZip = "37037";
    userInfo.MainLocationCityState = "CHRISTIANA, TN";
    userInfo.ProducerId = "26";
    userInfo.StateId = "73";
    userInfo.IsCurrentlyInsured = "1";
    userInfo.WhenIsCoverageNeeded = "1";
    userInfo.CoverageLimits = "1";
    userInfo.PolicyType = "1";
    userInfo.PolicyTerm = "6";
    userInfo.RatingVersionId = "40";
    userInfo.CompanyId = "13";
    userInfo.PaymentPlanId = "377";
    userInfo.DepositPercent = "25.00";
    userInfo.InstallmentCount = "5";
    userInfo.EmailAddress = "brandonw@atiba.com";
    userInfo.EffectiveDate = "2014-06-16 00:01:00";
    userInfo.PhoneNumber = "31454098881";
    userInfo.StreetAddress = "123 Main";
    userInfo.AptNumber = "";
    userInfo.CreditScoreEntered = "Excellent";
    userInfo.ZipCode = "37174";
    userInfo.City = "SPRING HILLs";
    userInfo.BodilyInjury = "25,000/50,0000";
    userInfo.PropertyDamage = "25,000";
    userInfo.UninsuredBodilyInjury = "0";
    userInfo.UninsuredPropertyDamage = "0";
    userInfo.Comprehensive = "500";
    userInfo.Collision = "500";
    userInfo.AccidentalDeath = "0";
    userInfo.TowingAndLabor ="0";
    //"RentalReimbursement": $("#medicalEachPerson option:selected").val();
    //"PersonalInjury": $("#medicalEachPerson option:selected").val();
    userInfo.PersonalInjuryEachPerson = "0";
    userInfo.PolicyNumber = "";
    userInfo.PolicyId = "";
}

setParams();

function searchZip() {
    url = service + "/zip";

    var params = { 'zip': $("#zip").val(), 'producerId': $("#producerID").val() };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        data: JSON.stringify(params),
        success: function (data) {
            console.log(data);
            userInfo.ProducerId = $("#producerId").val();
            userInfo.ZipCode = data.Zips[0].Zip;
            userInfo.MainLocationCityState = data.Zips[0].CityStateDisplay;
            userInfo.RatingVersionId = data.RatingVersionId;
            userInfo.StateId = data.Zips[0].StateId;
            userInfo.City = data.Zips[0].City;
        }
    });
}

function searchZipNoProducer() {
    url = service + "/zip";

    var params = { 'zip': $("#zip").val() };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        crossDomain: true,
        timeout: 10000,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        success: function (response) {
            console.log(response);
        }
    });
}

function searchMake() {
    url = service + "/vehicle";

    var params = { 'action': 'make', 'yearModel': $("#yearModel").val() };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        data: JSON.stringify(params),
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        success: function (response) {
            console.log(response);
        }
    });
}

function searchModel() {
    url = service + "/vehicle";

    var params = { 'action': 'model', 'yearModel': $("#yearModel").val(), 'makeName': $("#makeName").val() };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(params),
        success: function (response) {
            console.log(response);
        }
    });
}

function searchStyle() {
    url = service + "/vehicle";

    var params = { 'action': 'style', 'yearModel': $("#yearModel").val(), 'makeName': $("#makeName").val(), 'modelName': $("#modelName").val() };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(params),
        success: function (response) {
            console.log(response);
        }
    });
}

function searchSymbol() {
    url = service + "/vehicle";

    var params = { 'action': 'symbol', 'styleID': $("#yearModel").val(), 'makeName': $("#makeName").val(), 'modelName': $("#modelName").val() };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(params),
        success: function (response) {
            console.log(response);
        }
    });
}

function searchVIN() {
    url = service + "/vehicle";

    var params = { 'action': 'vin', 'vin': $("#vin").val() };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(params),
        success: function (response) {
            console.log(response);
        }
    });
}

function Coverage() {
    url = service + "/coverage";

    var params = { '9': $("#ratingVersionID").val(), 'requireSpecialCoverages': $("#requireSpecialCoverages").val() };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(params),
        success: function (response) {
            console.log(response);
            $("#divResultCoverage").html(response);
            Quote();
        }
    });
}


function RatingPaymentPlan() {
    url = service + "/ratingPaymentPlan";

    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(getParams()),
        success: function (response) {
            console.log(response);
            $("#divResultContainer").html(response);
        }
    });
}

function Quote() {
    url = service + "/quote";

    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(userInfo),
        success: function (data) {
            console.log(data);
            sessionStorage.setItem("PolicyId", data.PolicyId);
            sessionStorage.setItem("PolicyNumber", data.PolicyNumber);
            sessionStorage.setItem("PerMonth", data.PolicyNumber);
            sessionStorage.setItem("OneTime", data.OneTime);
            sessionStorage.setItem("DownPayment", data.DownPayment);

            $("#divResultContainer").html(data);
        }
    });
}

function ESignature() {
    url = service + "/eSignature";

    userInfo.action = "signature";
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(userInfo),
        success: function (response) {
            console.log(response);
            $("#divResultContainer").html(response);
        }
    });
}

//function ESignaturePDF() {
//    url = service + "/eSignature";

//    var params = { 'action': 'pdf', 'name': 'Josh Owens', 'initials': 'JO', 'dateTime': '05/26/2014 05:32 pm', 'docPos': '1', 'policyID': '1351124' };
//    $.ajax({
//        url: url,
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        timeout: 10000,
//        crossDomain: true,
//        headers: { 'Access-Control-Allow-Origin': '*.*' },
//        data: JSON.stringify(params),
//        success: function (response) {
//            console.log(response);
//            $("#displayMVC").html(response);
//        }
//    });
//}

function Payment() {
    url = service + "/payment";




    var params = { 'action': 'payment' };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(params),
        success: function (response) {
            console.log(response);
            $("#divResultContainer").html(response);
        }
    });
}

function Process() {
    url = service + "/payment";
    var data = JSON.stringify($('form').serializeObject())
    $('#result').text(data);

    var params = { 'action': 'process', 'isLive': '0', 'form': data };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(params),
        success: function (response) {
            console.log(response);
            $("#divResultContainer").html(response);
        }
    });
}

function ESignaturePDFQuick() {
    url = "http://consumerdirect.jupiterautoins.net/api/eSignature";

    var params = { 'action': 'pdf', 'name': 'Josh Owens', 'initials': 'JO', 'dateTime': '05/26/2014 05:32 pm', 'docPos': '1', 'policyID': '1351311' };
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        crossDomain: true,
        headers: { 'Access-Control-Allow-Origin': '*.*' },
        data: JSON.stringify(params),
        success: function (response) {
            console.log(response);
            $("#divResultContainer").html(response);
        }
    });
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};