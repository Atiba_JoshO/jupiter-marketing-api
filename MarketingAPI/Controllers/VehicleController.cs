﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

using MarketingAPI.Models;
using MarketingAPI.Helper;
using MarketingAPI.BL;
using MarketingAPI.Object;

namespace MarketingAPI.Controllers
{
    public class VehicleController : ApiController
    {
        public IHttpActionResult Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        public IHttpActionResult PostVehicle(HttpRequestMessage request)
        {
            var content = request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            dynamic data = Utility.ParseJSON(jsonContent);

            if (data.action.Value == "make")
            {
                return Ok(Makes(data));
            } 
            else if(data.action.Value == "model")
            {
                return Ok(Models(data));
            }
            else if (data.action.Value == "style")
            {
                return Ok(Styles(data));
            }
            else if (data.action.Value == "symbol")
            {
                return Ok(Symbols(data));
            }
            else if (data.action.Value == "vin")
            {
                return Ok(VIN(data));
            }

            return Ok("error");
        }

        private List<string> Makes(dynamic data) {
            
            int yearModel = Convert.ToInt32(data.yearModel);

            MakeBL makeBL = new MakeBL(Utility.ConnectionString("JupiterMGA_ChromeData"));

            List<string> makes = makeBL.GetMake(yearModel);

            return makes;
        }

        private List<string> Models(dynamic data)
        {
            ModelBL modelBL = new ModelBL(Utility.ConnectionString("JupiterMGA_ChromeData"));

            List<string> models = modelBL.GetModel(Convert.ToInt32(data.yearModel), data.makeName.Value);

            return models;
        }

        private List<StyleResponseItem> Styles(dynamic data)
        {
            StyleBL styleBL = new StyleBL(Utility.ConnectionString("JupiterMGA_ChromeData"));

            List<StyleResponseItem> styles = styleBL.GetStyles(Convert.ToInt32(data.yearModel), data.makeName.Value, data.modelName.Value);

            return styles;
        }

        private VehicleSymbol Symbols(dynamic data)
        {
            StyleBL styleBL = new StyleBL(Utility.ConnectionString("JupiterMGA_ChromeData"));

            VehicleSymbol symbols = styleBL.GetStyleSymbol(Convert.ToInt32(data.yearModel), Convert.ToInt32(data.styleId));

            return symbols;
        }

        private string VIN(dynamic data)
        
        {
            
            StyleBL styleBL = new StyleBL(Utility.ConnectionString("JupiterMGA_ChromeData"));

            VehicleSymbol symbols = styleBL.GetStyleSymbol(Convert.ToInt32(data.yearModel), Convert.ToInt32(data.styleId));

            ChromeService.Service chromeService = new ChromeService.Service();



            //var vinInfo = chromeService.CheckVin( 26, data.vin.Value);

            
           return "";
        }
    }
}
