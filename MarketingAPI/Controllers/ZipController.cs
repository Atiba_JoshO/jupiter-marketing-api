﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

using Newtonsoft.Json.Linq;

using MarketingAPI.Models;
using MarketingAPI.Helper;
using MarketingAPI.BL;
using MarketingAPI.Object;


namespace MarketingAPI.Controllers
{
    // [EnableCors(origins: "http://devdirect.jupiterautoins.net,http://marketing.jupiterautoins.net,http://consumerdirect.jupiterautoins.net,http://localhost:3547,http://localhost:31323", headers: "*", methods: "*")]
    // [EnableCors(origins: "*", headers: "*", methods: "*")] 
    public class ZipController : ApiController
    {
        public IHttpActionResult Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        public IHttpActionResult PostZip(HttpRequestMessage request)
        {
            var content = request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            dynamic data = Utility.ParseJSON(jsonContent);
            int producerID = 0;
          
            ZipBL zipBL = new ZipBL(Utility.ConnectionString("JupiterMGA"));

            try
            {
                producerID = Convert.ToInt32(data.producerId.Value);
            }
            catch
            {
                //  producerID doesn't exist
            } 
        
            if (producerID == 0)
            {
                List<ZipResponseItem> zips = new List<ZipResponseItem>();

                zips = zipBL.CheckZip(data.zip.Value, producerID);

                return Ok(zips);
            }
            else
            {
                ZipResponse zipResponse = new ZipResponse();
                
                zipResponse.RatingVersionID = zipBL.GetRatingVersionID(producerID);
                zipResponse.Zips = zipBL.CheckZip(data.zip.Value, producerID);
                zipResponse.Highway = zipBL.IsHighway(producerID);
                zipResponse.RedirectURL = zipBL.GetRedirectURL(producerID, data.zip.Value);

                if (zipResponse.Zips.Count == 0 || zipResponse.Zips == null)
                {
                    if (data.zip.Value == "99510")
                    {
                        zipResponse.Action = (int)ZipAction.SureHits;
                    } 
                    else 
                    {
                        zipResponse.Action = (int)ZipAction.MediaAlpha;
                    }
                }
                else
                {
                    zipResponse.Action = (int)ZipAction.Rate;
                }

                return Ok(zipResponse);
            }
   
            
        }
    }
}
