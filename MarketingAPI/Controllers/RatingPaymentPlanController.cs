﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

using MarketingAPI.Models;
using MarketingAPI.Helper;
using MarketingAPI.BL;
using MarketingAPI.Object;

namespace MarketingAPI.Controllers
{
    public class RatingPaymentPlanController : ApiController
    {
        public IHttpActionResult Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        public IHttpActionResult PostRatingPaymentPlan(HttpRequestMessage request)
        {
            var content = request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            dynamic data = Utility.ParseJSON(jsonContent);

            RatingBL ratingBL = new RatingBL(Utility.ConnectionString("JupiterMGA"), data);

            RatingPaymentPlan ratingPaymentPlan = new RatingPaymentPlan();

            ratingPaymentPlan.Rating = ratingBL.GetRating((int)data.ProducerId, (int)data.StateId, (string)data.PolicyType, (DateTime)data.EffectiveDate);
            ratingPaymentPlan.PaymentPlan = ratingBL.GetPayment(ratingPaymentPlan.Rating.RatingVersionId, (int)data.StateId, (string)data.PolicyType, (DateTime)data.EffectiveDate);

            return Ok(ratingPaymentPlan);
        }
    }
}
