﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.IO;
using System.Web.WebPages;
using System.Web.Razor.Parser;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using MarketingAPI.Models;
using MarketingAPI.Helper;
using MarketingAPI.BL;
using MarketingAPI.Object;
using MarketingAPI.DevInsuredPortal;


using Newtonsoft.Json.Linq;

namespace MarketingAPI.Controllers
{
    public class QuoteController : ApiController
    {
        public IHttpActionResult Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        public IHttpActionResult PostQuote(HttpRequestMessage request)
        {
            var content = request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            JObject payload = JObject.Parse(jsonContent);

            QuoteResults quoteResults = new QuoteResults();

            XmlDocument xmlDoc = Quote(payload, @"C:\repo\temp.xml");

            quoteResults.PolicyId = XMLValue(xmlDoc, "coveragePackage/policyID");
            quoteResults.PolicyNumber = XMLValue(xmlDoc, "coveragePackage/policyNum");
            quoteResults.PerMonth = XMLValue(xmlDoc, "coveragePackage/installmentAmount");
            quoteResults.DownPayment = XMLValue(xmlDoc, "coveragePackage/depositAmount");
            quoteResults.OneTime = Convert.ToString(Convert.ToDouble(XMLValue(xmlDoc, "coveragePackage/premiumTotal")) + Convert.ToDouble(XMLValue(xmlDoc, "coveragePackage/sr22Fees")) + Convert.ToDouble(XMLValue(xmlDoc, "coveragePackage/policyCharge")));
            

            return Ok(quoteResults);
        }

        public IHttpActionResult GetQuote(HttpRequestMessage request)
        {
            var content = request.Content;
            //string jsonContent = content.ReadAsStringAsync().Result;
            //JObject data = JObject.Parse(jsonContent);
            
            //dynamic data = Utility.ParseJSON(jsonContent);

            //Quote quote = new Quote();
            //quote.Vehicles = data.ToObject<List<Vehicle>>();
            //quote.Drivers = data.Drivers;

            //CreatePO(@"C:\repo\temp.xml");
            
            var model = new Quote();
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewPath = HttpContext.Current.Server.MapPath(@"~/Views");
            //response.Content = new StringContent(RazorEngineRender.RenderPartialViewToString(viewPath, "Quote.cshtml", model), Encoding.UTF8, "text/html");
            //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");

            return Ok(RazorEngineRender.RenderPartialViewToString(viewPath, "Quote.cshtml", model));
        }

        private XmlDocument Quote(JObject payload, string filename)
        {
            // Creates an instance of the XmlSerializer class;
            // specifies the type of object to serialize.
            XmlSerializer serializer = new XmlSerializer(typeof(Policy));
            StringWriter sw = new StringWriter(); 
            //TextWriter writer = new StreamWriter(filename);
            Policy policy = new Policy();
            dynamic insured = payload["Drivers"][0];

            string EffectiveDate;
            if(String.IsNullOrEmpty((string)payload["EffectiveDate"]))
            {
                EffectiveDate = DateTime.Now.ToString();
            }
            else 
            {
                EffectiveDate = (string)payload["EffectiveDate"];
            }
               
            // insured            
            policy.fname1 = insured.FirstName;
            policy.middle1 = insured.Middle;
            policy.lname1 = insured.LastName;
            policy.fname2 = String.Empty;
            policy.middle2 = String.Empty;
            policy.lname2 = String.Empty;
            policy.insuredName = String.Format("{0} {1}", insured.FirstName, insured.LastName);
            policy.companyID = (string)payload["CompanyId"];
            policy.effectiveDate = EffectiveDate;
            policy.producerID = (string)payload["ProducerId"];
            policy.policyTerm = (string)payload["PolicyTerm"];
            policy.policyType = (string)payload["PolicyType"];
            policy.paymentPlanID = (string)payload["PaymentPlanId"];
            policy.depositPercent = (string)payload["DepositPercent"];
            policy.installmentCount = (string)payload["InstallmentCount"];
            policy.stateID = (string)payload["StateId"];
            policy.zip = (string)payload["Zip"];
            policy.discount = "rO0ABXNyACBjb2xkZnVzaW9uLnJ1bnRpbWUuU3RydWN0V3JhcHBlcnFpXTdWtUQxAgABTAADbWFwdAAPTGphdmEvdXRpbC9NYXA7eHBzcgARamF2YS51dGlsLkhhc2hNYXAFB9rBwxZg0QMAAkYACmxvYWRGYWN0b3JJAAl0aHJlc2hvbGR4cD9AAAAAAAAMdwgAAAAQAAAAAXQACHByaW9ySW5zdAACb254";
            policy.creditScoreEntered = (string)payload["CreditScoreEntered"];
            policy.howSoon = EnumUtility.GetDescriptionFromValue((WhenIsCoverageNeeded)Convert.ToInt32(payload["WhenIsCoverageNeeded"]), typeof(WhenIsCoverageNeeded));
            policy.PrefCoverageLimit = EnumUtility.GetDescriptionFromValue((CoverageLimits)Convert.ToInt32(payload["CoverageLimits"]), typeof(CoverageLimits));
            policy.address1 = (string)payload["StreetAddress"];
            policy.address2 = (string)payload["AptNumber"];
            policy.city = (string)payload["City"];
            policy.email = (string)payload["EmailAddress"];
            policy.phone = (string)payload["PhoneNumber"];
            policy.state = payload["MainLocationCityState"].ToString().Split(new char[] { @","[0] }).Last().Trim();
            

            // drivers
            List<driver> driverList = new List<driver>();

            foreach(dynamic d in payload["Drivers"])
            {
                driver drivers = new driver();
                drivers.fname = d.FirstName;
                drivers.lname = d.LastName;
                drivers.age = Utility.CalculateAge((DateTime)d.DOB);
                drivers.gender = EnumUtility.GetDescriptionFromValue((DriverGender)Convert.ToInt32(d.Gender), typeof(DriverGender));
                drivers.maritalStatus = EnumUtility.GetDescriptionFromValue((IsMarried)Convert.ToInt32(d.IsMarried), typeof(IsMarried));
                drivers.Excluded = d.Excluded;
                drivers.sdipPoints = d.SDIPPoints;
                driverList.Add(drivers);
            }
            policy.drivers = driverList;
            
            //vehicles
            List<vehicle> vehicleList = new List<vehicle>();
            foreach(dynamic v in payload["Vehicles"])
            {
                vehicle vehicles = new vehicle();
                vehicles.vin = v.VIN;
                vehicles.vehicleYear = v.Year;
                vehicles.make = v.Make;
                vehicles.model = v.Model;
                vehicles.bodyType = v.Style;
                vehicles.fullCoverage = v.IsFullCoverage;
                vehicles.ownership = v.IsPaid;
                vehicles.symbol_liab = v.Collision;
                vehicles.symbol_comp = v.Comprehensive;
                vehicles.symbol_coll = v.Collision;
                vehicles.usage = v.Usage;
                vehicles.VINState = v.VINState;
                vehicles.validVIN = v.ValidVIN;
                vehicleList.Add(vehicles);
            }
            policy.vehicles = vehicleList;
            
            //coverage package
            CoveragePackage coveragePackage = new CoveragePackage();
            CoveragePackages coveragePackages = new CoveragePackages();
            coveragePackage.id = "Basic";
            coveragePackage.coveragePackageID = "10";
            coveragePackage.BIEachAccident = CheckNoCoverage(payload["BodilyInjury"].ToString().Split(new char[] { @"\"[0], "/"[0] }).Last().Trim());
            coveragePackage.BIEachPerson = CheckNoCoverage(payload["BodilyInjury"].ToString().Split(new char[] { @"\"[0], "/"[0] }).First().Trim());
            coveragePackage.PDEachAccident = CheckNoCoverage((string)payload["PropertyDamage"]);
            coveragePackage.uninsuredBIEachPerson = CheckNoCoverage(payload["UninsuredBodilyInjury"].ToString().Split(new char[] { @"\"[0], "/"[0] }).First().Trim());
            coveragePackage.uninsuredBIEachAccident = CheckNoCoverage(payload["UninsuredBodilyInjury"].ToString().Split(new char[] { @"\"[0], "/"[0] }).Last().Trim());
            coveragePackage.uinsuredPDEachAccident = CheckNoCoverage((string)payload["UninsuredPropertyDamage"]);
            coveragePackage.medicalEachPerson = CheckNoCoverage((string)payload["Medical"]);
            coveragePackage.comprehensiveDeductible = CheckNoCoverage((string)payload["Comprehensive"]);
            coveragePackage.collisionDeductible = CheckNoCoverage((string)payload["Collision"]);
            coveragePackage.accidentalDeathEachPerson = CheckNoCoverage((string)payload["AccidentalDeath"]);
            coveragePackage.towingAndLabor = CheckNoCoverage((string)payload["TowingAndLabor"]);
            coveragePackage.rentalReimbursement = "0";
            coveragePackage.PIPOption = "0";
            coveragePackage.PIPEachPerson = CheckNoCoverage((string)payload["PersonalInjuryEachPerson"]);
            coveragePackages.coveragePackage = coveragePackage;
            policy.coveragePackages = coveragePackages;

            //serializer.Serialize(writer, policy);
            //writer.Close();
            serializer.Serialize(sw, policy); 

            InsuredPortalService insuredPortalService = new InsuredPortalService();
            string usr = System.Configuration.ConfigurationManager.AppSettings.Get("insuredPortalUID");
            string pwd = System.Configuration.ConfigurationManager.AppSettings.Get("insuredPortalPWD");

            var xml = insuredPortalService.getXML(usr, pwd, sw.ToString());
            
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml((((System.Xml.XmlNode[])(xml))[2]).InnerXml);

            return xmlDoc;
        }

        private String XMLValue(XmlDocument xmlDoc, string path)
        {
            var node = xmlDoc.SelectSingleNode(path);

            return node.InnerText;
        }

        private String CheckNoCoverage(string value)
        {
            if (String.IsNullOrEmpty(value) || value == "No Coverage")
                return "0";

            return value.Replace("$", "").Replace(",", "");
            
        }
    }
}
