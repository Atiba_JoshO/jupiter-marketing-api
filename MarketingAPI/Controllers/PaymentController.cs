﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.IO;
using System.Web.WebPages;
using System.Web.Razor.Parser;
using System.Web;
using System.Web.Mvc;
using System.Text;
using MarketingAPI.Models;
using MarketingAPI.Helper;
using MarketingAPI.BL;
using MarketingAPI.Object;

using Newtonsoft.Json.Linq;

namespace MarketingAPI.Controllers
{
    public class PaymentController : ApiController
    {
        public IHttpActionResult Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        public IHttpActionResult PostPayment(HttpRequestMessage request)
        {
            var content = request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            dynamic data = Utility.ParseJSON(jsonContent);

            if (data.action.Value == "payment")
            {
                return Ok(Payment(data));
            }
            else if (data.action.Value == "process")
            {
                return Ok(Process(request));
            }

            return Ok("error");
        }

        private String Payment(dynamic data)
        {
            Payment payment = new Payment();
            payment.CyberSource = new PaymentBL().GetConfig("12");

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewPath = HttpContext.Current.Server.MapPath(@"~/Views");
            
            return RazorEngineRender.RenderPartialViewToString(viewPath, "Payment.cshtml", payment);
        }

        private String Process(HttpRequestMessage request)
        {
            var content = request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            JObject data = JObject.Parse(jsonContent);
            dynamic form = Utility.ParseJSON(((Newtonsoft.Json.Linq.JValue)(data["form"])).Value.ToString());

            PaymentProcess paymentProcess = new PaymentProcess();
            paymentProcess.FormField = form;
            paymentProcess.CyberSource = new PaymentBL().GetConfig("12");
            paymentProcess.CyberSourceEndPoint = new PaymentBL().GetCyberSource(Convert.ToBoolean(Convert.ToInt32(data["isLive"])));

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewPath = HttpContext.Current.Server.MapPath(@"~/Views");

            return RazorEngineRender.RenderPartialViewToString(viewPath, "PaymentProcess.cshtml", paymentProcess);
        }
    }
}
