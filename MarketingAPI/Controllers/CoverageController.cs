﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.IO;
using System.Web.WebPages;
using System.Web.Razor.Parser;
using System.Web;
using System.Web.Mvc;
using System.Text;

using MarketingAPI.Models;
using MarketingAPI.Helper;
using MarketingAPI.BL;
using MarketingAPI.Object;


namespace MarketingAPI.Controllers
{
    //[EnableCors(origins: "http://devdirect.jupiterautoins.net,http://marketing.jupiterautoins.net,http://consumerdirect.jupiterautoins.net,http://localhost:3547,http://localhost:31323", headers: "*", methods: "*")]
    public class CoverageController : ApiController
    {
        public IHttpActionResult Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        public String PostCoverage(HttpRequestMessage request)
        {
            var content = request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            dynamic data = Utility.ParseJSON(jsonContent);

            Coverage model = new Coverage();


            model = GetModel("26", "0");
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewPath = HttpContext.Current.Server.MapPath(@"~/Views");            
            
            return RazorEngineRender.RenderPartialViewToString(viewPath, "Coverage.cshtml", model);
        }

        public String GetCoverage(HttpRequestMessage request)
        {
            NameValueCollection data = HttpUtility.ParseQueryString(request.RequestUri.Query);

            var model = GetModel("26", "0");
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewPath = HttpContext.Current.Server.MapPath(@"~/Views");            

            return RazorEngineRender.RenderPartialViewToString(viewPath, "Coverage.cshtml", model);
        }

        private Coverage GetModel(string ratingVersionID, string requireSpecialCoverages)
        {
            Coverage coverage = new Coverage();
            CoverageAvailable coverageAvailable = new CoverageAvailable();
            CoverageBL coverageBL = new CoverageBL(Utility.ConnectionString("JupiterMGA_Rate"), Convert.ToInt32(ratingVersionID));

            coverage.RatingVersionID = ratingVersionID;

            //Single Limit Coverage
            //coverage.SLEachAccident = coverageBL.GetCoverage(data.requireSpecialCoverages, "sl");

            //Bodily Injury Coverage
            coverage.BIEachPerson = coverageBL.GetCoverage(requireSpecialCoverages, "bi");
            //coverage.BIEachAccident = coverageBL.GetCoverage(requireSpecialCoverages, "bi");
            var eachPerson = coverage.BIEachPerson.Last();

            //Bodily Injury Each Accident & Property Damage Coverage
            dynamic topSubCoverage = coverageBL.GetAssociatedCoverage("bi", "Bodily Injury", eachPerson);
            coverage.BIEachAccident = topSubCoverage.Item1;
            coverage.PDEachAccident = topSubCoverage.Item2;

            //Uninsured Motorists Bodily Injury
            coverage.UninsuredBIEachPerson = coverageBL.GetCoverage(requireSpecialCoverages, "umbi");
            eachPerson = coverage.UninsuredBIEachPerson.Last();

            //Uninsured Motorists Bodily Injury Each Accident & Property Damage Coverage
            dynamic umbiTopSub = coverageBL.GetAssociatedCoverage("umbi", "Uninsured Motorist Bodily Injury", eachPerson);
            coverage.UninsuredBIEachAccident = umbiTopSub.Item1;
            coverage.UninsuredPDEachAccident = umbiTopSub.Item2;

            //Underinsured Motorists Bodily Injury
            coverage.UnderinsuredBIEachPerson = coverageBL.GetCoverage(requireSpecialCoverages, "unbi");
            eachPerson = coverage.UninsuredBIEachPerson.Last();

            //Underinsured Motorists Bodily Injury Each Accident & Property Damage Coverage
            dynamic unbiToSub = coverageBL.GetAssociatedCoverage("unbi", "Underinsured Motorist Bodily Injury", eachPerson);
            coverage.UnderinsuredBIEachAccident = umbiTopSub.Item1;
            coverage.UnderinsuredPDEachAccident = umbiTopSub.Item2;

            coverage.MedicalEachPerson = coverageBL.GetCoverage(requireSpecialCoverages, "mp");
            coverage.PIPLimit = coverageBL.GetCoverage(requireSpecialCoverages, "pip");           
            coverage.AccidentalDeathEachPerson = coverageBL.GetCoverage(requireSpecialCoverages, "add");
            coverage.WorkLossEachPerson = coverageBL.GetCoverage(requireSpecialCoverages, "wl");
            coverage.ComprehensiveDeductible = coverageBL.GetCoverage(requireSpecialCoverages, "comp");
            coverage.CollisionDeductible = coverageBL.GetCoverage(requireSpecialCoverages, "coll");            
            coverage.Rental = coverageBL.GetCoverage(requireSpecialCoverages, "re");
            coverage.TowingAndLabor = coverageBL.GetCoverage(requireSpecialCoverages, "tl");
            //coverage.TransportationExpenses = coverageBL.GetCoverage(data.requireSpecialCoverages, "te");
            //coverage.CustomEquipment = coverageBL.GetCoverage(data.requireSpecialCoverages, "ce");

        //    //coverage.UninsuredCSLEachAccident = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);
        //    //coverage.UninsuredCSLDeductible = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);
        //    //coverage.UninsuredBIDeductible = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);
        //    //coverage.UninsuredPDDeductible = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);
        //    //coverage.UnderinsuredCSLEachAccident = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);
        //    //coverage.OtherThanACV = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);
        //    //coverage.Loan = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);            
        //    //coverage.SubstituteBuyBack = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);
        //    //coverage.GlassCoverage = coverageBL.GetCoverage(data.requireSpecialCoverages, data.coverageType.Value);

            return coverage;
        }          
    }

    public class RazorEngineRender
    {
        public static string RenderPartialViewToString<T>(string templatePath, string viewName, T model)
        {
            string text = System.IO.File.ReadAllText(Path.Combine(templatePath, viewName));
            string renderedText = RazorEngine.Razor.Parse(text, model);
            return renderedText;
        }
    }
}
