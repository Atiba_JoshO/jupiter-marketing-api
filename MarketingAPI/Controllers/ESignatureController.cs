﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.IO;
using System.Web.WebPages;
using System.Web.Razor.Parser;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Security.Cryptography;


using MarketingAPI.Models;
using MarketingAPI.Helper;
using MarketingAPI.BL;
using MarketingAPI.Object;

using Newtonsoft.Json.Linq;

namespace MarketingAPI.Controllers
{
    public class ESignatureController : ApiController
    {
        public IHttpActionResult Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        public IHttpActionResult PostESignature(HttpRequestMessage request)
        {
            var content = request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            dynamic data = Utility.ParseJSON(jsonContent);

            if (data.action.Value == "signature")
            {
                return Ok(Signature(data));
            }
            else if (data.action.Value == "document")
            {
                return Ok(Document(data));
            }
            else if (data.action.Value == "doc")
            {
                return Ok(Hash(data));
            } 

            return Ok("error");            
        }

        private String Signature(dynamic data)
        {
            ESignature eSignature = new ESignature();

            eSignature.SecurityQuestion1 = new EsignatureBL().GetSecurityQuestion("1");
            eSignature.SecurityQuestion2 = new EsignatureBL().GetSecurityQuestion("2");

            List<Driver> drivers = new List<Driver>();

            foreach (dynamic d in data["Drivers"])
            {
                Driver driver = new Driver();
                driver.FirstName = d.FirstName;
                driver.LastName = d.LastName;
                driver.MiddleName = d.MiddleName;
                drivers.Add(driver);
            }

            eSignature.Drivers = drivers;

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewPath = HttpContext.Current.Server.MapPath(@"~/Views");
            
            return RazorEngineRender.RenderPartialViewToString(viewPath, "ESignature.cshtml", eSignature);
        }

        private String Document(dynamic data)
        {
            ESignaturePDF eSignaturePDF = new ESignaturePDF();
            eSignaturePDF.PolicyID = Convert.ToInt32(data.policyID);
            eSignaturePDF.Name = data.name;
            eSignaturePDF.Initials = data.initials;
            eSignaturePDF.DocPos = data.docPos;
            eSignaturePDF.SignatureDateTime = Convert.ToDateTime(data.dateTime);

            eSignaturePDF.PdfUrl = String.Format(@"http://devapp.jupiterautoins.net/service/external/insured2/eSigDirect.cfc?method=getHTMLTOPDFDOC&accessString={0}&ptsName={1}&ptsSystem={2}&policyID={3}&docPos=1&eSigName={4}&eSigInitials={5}&eSigDateTime=05/26/2014 05:32 pm&returnFormat=plain", Hash(data), System.Configuration.ConfigurationManager.AppSettings.Get("ptsName"), System.Configuration.ConfigurationManager.AppSettings.Get("ptsSystem"), data["policyID"], data["name"], data["initials"]);
            eSignaturePDF.HtmlUrl = String.Format(@"http://devapp.jupiterautoins.net/service/external/insured2/eSigDirect.cfc?method=getHTML&accessString={0}&ptsName={1}&ptsSystem={2}&policyID={3}&docPos=1&eSigName={4}&eSigInitials={5}&eSigDateTime=05/26/2014 05:32 pm&returnFormat=plain", Hash(data), System.Configuration.ConfigurationManager.AppSettings.Get("ptsName"), System.Configuration.ConfigurationManager.AppSettings.Get("ptsSystem"), data["policyID"], data["name"], data["initials"]);


            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewPath = HttpContext.Current.Server.MapPath(@"~/Views");

            return RazorEngineRender.RenderPartialViewToString(viewPath, "ESignaturePDF.cshtml", eSignaturePDF);
        }

        private string Hash(dynamic data)
        {
            ESignaturePDF eSignaturePDF = new ESignaturePDF();
            eSignaturePDF.PolicyID = Convert.ToInt32(data.policyID);
            eSignaturePDF.Name = data.name;
            eSignaturePDF.Initials = data.initials;
            eSignaturePDF.DocPos = data.docPos;
            eSignaturePDF.SignatureDateTime = Convert.ToDateTime(data.dateTime);

            string listToHash = String.Format("{0},{1},{2}", data["policyID"], System.Configuration.ConfigurationManager.AppSettings.Get("ptsName"), System.Configuration.ConfigurationManager.AppSettings.Get("ptsSystem"));

            string hash = sign(listToHash, System.Configuration.ConfigurationManager.AppSettings.Get("serverKey"));

            //"testing", System.Configuration.ConfigurationManager.AppSettings.Get("serverKey").ToString()
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            //request.Method = "GET";
            //var encoding = new UTF8Encoding();
            //HttpWebResponse resp = (HttpWebResponse)request.GetResponse();
            //Stream responseStream = resp.GetResponseStream();
            //StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8);
            //string doc = readStream.ReadToEnd();
            //HttpContext.Response.AddHeader("content-disposition", "attachment; filename=form.pdf");
            //return new FileStreamResult(responseStream, "text/html"); 

            return hash;            
        }

        private String sign(String data, String secretKey)
        {
            UTF8Encoding encoding = new System.Text.UTF8Encoding();
            byte[] keyByte = encoding.GetBytes(secretKey);

            HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);
            byte[] messageBytes = encoding.GetBytes(data);
            return Convert.ToBase64String(hmacsha256.ComputeHash(messageBytes));
        }
    }
}
