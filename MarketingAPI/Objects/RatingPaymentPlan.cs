﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class RatingPaymentPlan
    {
        public RatingInfo Rating { get; set; }
        public PaymentPlanInfo PaymentPlan { get; set; }
    }

    public class RatingInfo : IHydratableFromDataRecord
    {
        public RatingInfo()
        {

        }

        public int RatingVersionId { get; set; }
        public int CompanyId { get; set; }
        public string PolicyTerm { get; set; }

        public void HydrateFromDataRecord(System.Data.IDataRecord record)
        {
            RatingVersionId = NullConverter.NoNullInts(record["ratingVersionID"]);
            CompanyId = NullConverter.NoNullInts(record["companyID"]);
            PolicyTerm = NullConverter.NoNullStrings(record["policyTerm"]);
        }
    }

    public class PaymentPlanInfo : IHydratableFromDataRecord
    {
        public PaymentPlanInfo()
        {

        }

        public int PaymentPlanId { get; set; }
        public int InstallmentCharge { get; set; }
        public int InstallmentCount { get; set; }
        public int DepositPercent { get; set; }

        public void HydrateFromDataRecord(System.Data.IDataRecord record)
        {
            PaymentPlanId = NullConverter.NoNullInts(record["paymentPlanID"]);
            InstallmentCharge = NullConverter.NoNullInts(record["installmentCharge"]);
            InstallmentCount = NullConverter.NoNullInts(record["installmentCount"]);
            DepositPercent = NullConverter.NoNullInts(record["depositPercent"]);
        }
    }
}