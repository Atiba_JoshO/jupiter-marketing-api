﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;



namespace MarketingAPI.Object
{

    // The XmlRootAttribute allows you to set an alternate name 
    // (PurchaseOrder) for the XML element and its namespace. By 
    // default, the XmlSerializer uses the class name. The attribute 
    // also allows you to set the XML namespace for the element. Lastly,
    // the attribute sets the IsNullable property, which specifies whether 
    // the xsi:null attribute appears if the class instance is set to 
    // a null reference.
    [XmlRootAttribute("policy", IsNullable = false)]
    public class Policy
    {
        public String fname1;
        public String middle1;
        public String lname1;

        public String fname2;
        public String middle2;
        public String lname2;

        public String insuredName;
        public String companyId;
        public String effectiveDate;

        public String companyID;

        public String producerID;
        public String policyTerm;
        public String policyType;
        public String paymentPlanID;

        public String depositPercent;
        public String installmentCount;
        public String stateID;

        public String zip;
        public String discount;
        public String creditScoreEntered;

        public String howSoon;
        public String PrefCoverageLimit;
        public String address1;

        public String address2;
        public String city;
        public String email;

        public String phone;
        public String state;
        
        public List<driver> drivers;

        public List<vehicle> vehicles;

        public CoveragePackages coveragePackages;
    }

    public class driver
    {
        public String fname;
        public String lname;
        public String age;
        public String gender;
        public String maritalStatus;
        public String Excluded;
        public String sdipPoints;
    }

    public class vehicle 
    {
        public String vin;
        public String vehicleYear;
        public String make;
        public String model;
        public String bodyType;
        public String ownership;
        public String fullCoverage;
        public String symbol_liab;
        public String symbol_comp;
        public String symbol_coll;
        public String usage;
        public String VINState;
        public String validVIN;
    }

    public class CoveragePackages
    {
        public CoveragePackage coveragePackage;
    }

    public class CoveragePackage
    {
        [XmlAttribute]
        public String id;

        public String coveragePackageID;
        public String BIEachPerson;
        public String BIEachAccident;
        public String PDEachAccident;
        public String uninsuredBIEachPerson;
        public String uninsuredBIEachAccident;
        public String uinsuredPDEachAccident;
        public String medicalEachPerson;
        public String comprehensiveDeductible;
        public String collisionDeductible;
        public String accidentalDeathEachPerson;
        public String towingAndLabor;
        public String rentalReimbursement;
        public String PIPOption;
        public String PIPEachPerson;

    }
}


