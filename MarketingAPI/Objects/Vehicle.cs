﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class Vehicle
    {
        public String Year { get; set; }

        public String Make { get; set; }

        public String Model { get; set; }

        public String Style { get; set; }

        public IsPaid IsPaid { get; set; }

        public IsFullCoverage IsFullCoverage { get; set; }
    }

}