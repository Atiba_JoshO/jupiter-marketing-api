﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class ZipResponseItem : IHydratableFromDataRecord
    {
        public ZipResponseItem()
        {

        }

        public String Zip { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String StateID { get; set; }
        public String CityStateDisplay
        {
            get { return City + ", " + State; }
        }

        public void HydrateFromDataRecord(System.Data.IDataRecord record)
        {
            Zip = NullConverter.NoNullStrings(record["ZIPCODE"]);
            City = NullConverter.NoNullStrings(record["CITY"]);
            State = NullConverter.NoNullStrings(record["ABBR"]);
            StateID = NullConverter.NoNullStrings(record["StateID"]);
        }
    }
}