﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class StyleResponseItem : IHydratableFromDataRecord
    {
        public StyleResponseItem()
        {

        }

        public int ID { get; set; }

        public string Name { get; set; }

        public void HydrateFromDataRecord(System.Data.IDataRecord record)
        {
            ID = NullConverter.NoNullInts(record["styleID"]);
            Name = NullConverter.NoNullStrings(record["styleName"]);
        }
    }

    public class Styles : IEnumerable<StyleResponseItem>
    {
        public StyleResponseItem style { get; set; }

        public IEnumerator<StyleResponseItem> GetEnumerator()
        {
            yield return style;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}