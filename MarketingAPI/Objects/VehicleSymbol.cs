﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class VehicleSymbol : IHydratableFromDataRecord
    {
        public VehicleSymbol()
        {

        }

        public String Liability { get; set; }
        public String Comprehensive { get; set; }
        public String Collision { get; set; }

        public void HydrateFromDataRecord(System.Data.IDataRecord record)
        {
            Liability = NullConverter.NoNullStrings(record["liabilitySymbol"]);
            Comprehensive = NullConverter.NoNullStrings(record["comprehensiveSymbol"]);
            Collision = NullConverter.NoNullStrings(record["collisionSymbol"]);
        }
    }
}