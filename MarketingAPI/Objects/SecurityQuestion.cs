﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class SecurityQuestion
    {
        public int ID { get; set; }

        public string Question { get; set; }
    }
}