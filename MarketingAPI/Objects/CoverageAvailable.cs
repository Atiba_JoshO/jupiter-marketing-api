﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class CoverageAvailable
    {
        public List<string> listOfferBodilyInjury { get; set; }
        public List<string> listOfferSingleLimit { get; set; }
        public List<string> listOfferPropertyDamage { get; set; }
        public List<string> listOfferMedicalPayments { get; set; }
        public List<string> listOfferPIP { get; set; }
        public List<string> listOfferComprehensive { get; set; }
        public List<string> listOfferCollision { get; set; }
        public List<string> listOfferLoan { get; set; }
        public List<string> listOfferUMBI { get; set; }
        public List<string> listOfferUMPD { get; set; }
        public List<string> listOfferUMCSL { get; set; }
        public List<string> listOfferUNBI { get; set; }
        public List<string> listOfferUNPD { get; set; }
        public List<string> listOfferUNCSL { get; set; }
        public List<string> listOfferWorkLoss { get; set; }
        public List<string> listOfferPDExclusion { get; set; }
        public List<string> listOfferAccidentalDeath { get; set; }
        public List<string> listOfferAVE { get; set; }
        public List<string> listOfferMedia { get; set; }
        public List<string> listOfferCustomEquip { get; set; }
        public List<string> listOfferComprehensiveDeductible { get; set; }
        public List<string> listOfferCollisionDeductible { get; set; }
        public List<string> listOfferTowing { get; set; }
        public List<string> listOfferTowingLimit { get; set; }
        public List<string> listOfferRental { get; set; }
        public List<string> listOfferTransportationExp { get; set; }
        public List<string> listOfferExtraDeductible { get; set; }
        public List<string> listOfferDirectRepair { get; set; }
        public List<string> listOfferHomeOwnerDiscount { get; set; }
        public List<string> listOfferTransferDiscount { get; set; }
        public List<string> listOfferEFTDiscount { get; set; }
        public List<string> listOfferRenewalDiscount { get; set; }
        public List<string> listOfferMultiCar { get; set; }
        public List<string> listFullCoverageDiscount { get; set; }
        public List<string> listOfferSubstituteBuyBack { get; set; }
        public List<string> listOfferGlassCoverage { get; set; }
    }
}