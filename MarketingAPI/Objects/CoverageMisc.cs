﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class CoverageValue
    {
        public string Name { get; set; }
        public string Abbr { get; set; }
        public string Value { get; set; }
    }

    public class CoveragesAvailable
    {
        public string Abbr { get; set; }
        public Boolean Available { get; set; }
        public Boolean Policy { get; set; }
        public Boolean Vehicle { get; set; }
        public Boolean Optional { get; set; }
    }

    public class TopCoverage : IHydratableFromDataRecord
    {
        public TopCoverage()
        {

        }
        public int AutoCoverageID { get; set; }
        public string PerAccident { get; set; }

        public void HydrateFromDataRecord(System.Data.IDataRecord record)
        {
            AutoCoverageID = NullConverter.NoNullInts(record["autoCoverageOptionID"]);
            PerAccident = NullConverter.NoNullStrings(record["perAccident"].ToString().Replace("x", ""));           
        }
    }

    public class SubCoverage : IHydratableFromDataRecord
    {
        public SubCoverage()
        {

        }
        public string PerPerson { get; set; }

        public string PerAccident { get; set; }

        public string DefaultValue { get; set; }

        public void HydrateFromDataRecord(System.Data.IDataRecord record)
        {
            PerPerson = NullConverter.NoNullStrings(record["perPerson"].ToString().Replace("x", ""));
            PerAccident = NullConverter.NoNullStrings(record["perAccident"].ToString().Replace("x", ""));
            DefaultValue = NullConverter.NoNullStrings(record["defaultValue"].ToString().Replace("x", ""));
        }
    }      
}