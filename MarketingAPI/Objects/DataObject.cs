﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace MarketingAPI.Object
{
    public abstract class DataObject
    {
        private readonly string _connectionString;
        private List<SqlParameter> _parameters = new List<SqlParameter>();

        protected DataObject()
        { }

        protected DataObject(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Add a SQL parameter for use with the next SQL command
        /// </summary>
        /// <param name="paramName">Parameter name</param>
        /// <param name="type">Parameter type</param>
        /// <param name="size">Parameter size</param>
        /// <param name="value">Parameter value</param>
        protected void SetParameter(string paramName, SqlDbType type, int size, object value)
        {
            SqlParameter param = new SqlParameter(paramName, type, size);
            param.Value = value;
            this._parameters.Add(param);
            //this.cmd.Parameters.Add(param);
        }

        /// <summary>
        /// Add a SQL parameter for use with the next SQL command
        /// </summary>
        /// <param name="paramName">Parameter name</param>
        /// <param name="type">Parameter type</param>
        /// <param name="value">Parameter value</param>
        protected void SetParameter(string paramName, SqlDbType type, object value)
        {
            SqlParameter param = new SqlParameter(paramName, type);
            param.Value = value;
            this._parameters.Add(param);
            //this.cmd.Parameters.Add(param);
        }

        /// <summary>
        /// Add a SQL parameter for use with the next SQL command
        /// </summary>
        /// <param name="paramName">Parameter name</param>
        /// <param name="value">Parameter value</param>
        protected void SetParameter(string paramName, object value)
        {
            SqlParameter param = new SqlParameter(paramName, value);
            this._parameters.Add(param);
        }

        /// <summary> Execute the sql to obtain a data reader</summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <param name="extendedTimeout">Flag indicating whether to use an extended timeout setting</param>
        /// <returns>Data reader against the command text</returns>
        protected IDataReader ExecuteDataReader(string commandText, CommandType commandType, IDataParameter[] parameters, bool extendedTimeout)
        {
            SqlConnection connection;
            if (extendedTimeout && _connectionString.IndexOf("Connection Timeout") == -1)
            {
                connection = new SqlConnection(_connectionString + ";Connection Timeout=1200");
            }
            else
            {
                connection = new SqlConnection(_connectionString);
            }
            IDbCommand command = connection.CreateCommand();
            connection.Open();

            command.CommandText = commandText;
            command.CommandType = commandType;
            if (extendedTimeout)
            {
                command.CommandTimeout = 1200;
            }

            if (parameters != null)
            {
                foreach (IDataParameter parameter in parameters)
                    command.Parameters.Add(parameter);
            }
            foreach (SqlParameter param in this._parameters)
                command.Parameters.Add(param);
            this._parameters = new List<SqlParameter>();

            IDataReader results = null;
            //try
            //{
            results = command.ExecuteReader(CommandBehavior.CloseConnection);
            //}
            //catch (SqlException ex)
            //{
            //   throw new QAException("4100", ex, ErrorMsgs.E4100_WHAT_HAPPENED_MESSAGE, 
            //}

            return results;
        }

        /// <summary> Execute the sql to obtain a data reader</summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <returns>Data reader against the command text</returns>
        protected IDataReader ExecuteDataReader(string commandText, CommandType commandType, IDataParameter[] parameters)
        {
            return ExecuteDataReader(commandText, commandType, parameters, false);
        }

        /// <summary> Execute the sql to obtain a data reader</summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <returns>Data reader against the command text</returns>
        protected IDataReader ExecuteDataReader(string commandText)
        {
            return ExecuteDataReader(commandText, CommandType.Text, null, false);
        }

        /// <summary> Execute the sql to obtain a data reader</summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <returns>Data reader against the command text</returns>
        protected IDataReader ExecuteDataReader(string commandText, IDataParameter[] parameters)
        {
            return ExecuteDataReader(commandText, CommandType.Text, parameters, false);
        }

        /// <summary> Execute the sql to obtain a data reader</summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <returns>Data reader against the command text</returns>
        protected IDataReader ExecuteDataReader(string commandText, CommandType commandType)
        {
            return ExecuteDataReader(commandText, commandType, null, false);
        }

        /// <summary> Execute the sql to obtain a data reader</summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="extendedTimeout">Flag indicating whether to use an extended timeout setting</param>
        /// <returns>Data reader against the command text</returns>
        protected IDataReader ExecuteDataReader(string commandText, CommandType commandType, bool extendedTimeout)
        {
            return ExecuteDataReader(commandText, commandType, null, extendedTimeout);
        }

        /// <summary>
        /// Execute the sql to obtain an object
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <param name="extendedTimeout">Flag indicating whether to use an extended timeout setting</param>
        /// <returns>An instance of T hydrated with data.</returns>
        protected T ExecuteDataReaderToObject<T>(string commandText, CommandType commandType, IDataParameter[] parameters, bool extendedTimeout) where T : IHydratableFromDataRecord, new()
        {
            T result = default(T);

            //try
            //{
            using (IDataReader reader = ExecuteDataReader(commandText, commandType, parameters, extendedTimeout))
            {
                if (reader.Read())
                {
                    result = CreateObjectFromDataRecord<T>(reader);
                }
            }
            //}
            //catch (SqlException ex)
            //{
            //   throw new QAException("4100", ex, ErrorMsgs.E4100_WHAT_HAPPENED_MESSAGE, 
            //}

            return result;
        }

        /// <summary>
        /// Execute the sql to obtain an object
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <returns>An instance of T hydrated with data.</returns>
        protected T ExecuteDataReaderToObject<T>(string commandText, CommandType commandType, IDataParameter[] parameters) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToObject<T>(commandText, commandType, parameters, false);
        }

        /// <summary>
        /// Execute the sql to obtain an object
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <returns>An instance of T hydrated with data.</returns>
        protected T ExecuteDataReaderToObject<T>(string commandText) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToObject<T>(commandText, CommandType.Text, null, false);
        }

        /// <summary>
        /// Execute the sql to obtain an object
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <returns>An instance of T hydrated with data.</returns>
        protected T ExecuteDataReaderToObject<T>(string commandText, CommandType commandType) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToObject<T>(commandText, commandType, null, false);
        }

        /// <summary>
        /// Execute the sql to obtain an object
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <returns>An instance of T hydrated with data.</returns>
        protected T ExecuteDataReaderToObject<T>(string commandText, IDataParameter[] parameters) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToObject<T>(commandText, CommandType.Text, parameters, false);
        }

        /// <summary>
        /// Execute the sql to obtain a list of objects
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <param name="extendedTimeout">Flag indicating whether to use an extended timeout setting</param>
        /// <returns>A list of T hydrated with data.</returns>
        protected IEnumerable<T> ExecuteDataReaderToList<T>(string commandText, CommandType commandType, IDataParameter[] parameters, bool extendedTimeout) where T : IHydratableFromDataRecord, new()
        {
            List<T> result = new List<T>();

            //try
            //{
            using (IDataReader reader = ExecuteDataReader(commandText, commandType, parameters, extendedTimeout))
            {
                while (reader.Read())
                {
                    result.Add(CreateObjectFromDataRecord<T>(reader));
                }
            }
            //}
            //catch (SqlException ex)
            //{
            //   throw new QAException("4100", ex, ErrorMsgs.E4100_WHAT_HAPPENED_MESSAGE, 
            //}

            return result;
        }

        /// <summary>
        /// Execute the sql to obtain a list of basic objects
        /// </summary>
        /// <typeparam name="T">A basic object/type like "string".</typeparam>
        /// <param name="fieldName">Sql field name to be added to the list.</param>
        /// <param name="commandText">Sql to be executed.</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <returns></returns>
        protected IEnumerable<T> ExecuteDataReaderToBasicList<T>(string fieldName, string commandText, CommandType commandType)
        {
            return ExecuteDataReaderToBasicList<T>(fieldName, commandText, commandType, null);
        }

        /// <summary>
        /// Execute the sql to obtain a list of basic objects
        /// </summary>
        /// <typeparam name="T">A basic object/type like "string".</typeparam>
        /// <param name="fieldName">Sql field name to be added to the list.</param>
        /// <param name="commandText">Sql to be executed.</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters.</param>
        /// <returns>A list of T.</returns>
        protected IEnumerable<T> ExecuteDataReaderToBasicList<T>(string fieldName, string commandText, CommandType commandType, IDataParameter[] parameters)
        {
            List<T> result = new List<T>();

            //try
            //{
            using (IDataReader reader = ExecuteDataReader(commandText, commandType, parameters))
            {
                while (reader.Read())
                {
                    result.Add((T)reader[fieldName]);
                }
            }
            //}
            //catch (SqlException ex)
            //{
            //   throw new QAException("4100", ex, ErrorMsgs.E4100_WHAT_HAPPENED_MESSAGE, 
            //}

            return result;
        }

        /// <summary>
        /// Execute the sql to obtain a list of objects
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <returns>A list of T hydrated with data.</returns>
        protected IEnumerable<T> ExecuteDataReaderToList<T>(string commandText, CommandType commandType, IDataParameter[] parameters) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToList<T>(commandText, commandType, parameters, false);
        }

        /// <summary>
        /// Execute the sql to obtain a list of objects
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <returns>A list of T hydrated with data.</returns>
        protected IEnumerable<T> ExecuteDataReaderToList<T>(string commandText) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToList<T>(commandText, CommandType.Text, null, false);
        }

        /// <summary>
        /// Execute the sql to obtain a list of objects
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <returns>A list of T hydrated with data.</returns>
        protected IEnumerable<T> ExecuteDataReaderToList<T>(string commandText, CommandType commandType) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToList<T>(commandText, commandType, null, false);
        }

        /// <summary>
        /// Execute the sql to obtain a list of objects
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <returns>A list of T hydrated with data.</returns>
        protected IEnumerable<T> ExecuteDataReaderToList<T>(string commandText, IDataParameter[] parameters) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToList<T>(commandText, CommandType.Text, parameters, false);
        }

        /// <summary>
        /// Execute the sql to obtain a list of objects
        /// </summary>
        /// <typeparam name="T">An object, with a constructor accepting IDataRecord as its parameter, to be hydrated.</typeparam>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="extendedTimeout">Flag indicating whether to use an extended timeout setting</param>
        /// <returns>A list of T hydrated with data.</returns>
        protected IEnumerable<T> ExecuteDataReaderToList<T>(string commandText, CommandType commandType, bool extendedTimeout) where T : IHydratableFromDataRecord, new()
        {
            return ExecuteDataReaderToList<T>(commandText, commandType, null, true);
        }

        private static T CreateObjectFromDataRecord<T>(IDataRecord record)
            where T : IHydratableFromDataRecord, new()
        {
            T result = new T();
            //try
            //{
            result.HydrateFromDataRecord(record);
            //}
            //catch (SqlException ex)
            //{
            //   throw new QAException("4100", ex, ErrorMsgs.E4100_WHAT_HAPPENED_MESSAGE, 
            //}

            return result;
        }

        /// <summary> Execute a non-select sql statement
        /// </summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <returns>Integer greater than 0 for success</returns>
        protected int ExecuteNonQuery(string commandText, CommandType commandType, IDataParameter[] parameters)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(commandText, connection) { CommandType = commandType };

                if (parameters != null)
                {
                    foreach (IDataParameter parameter in parameters)
                        command.Parameters.Add(parameter);
                }
                foreach (SqlParameter param in this._parameters)
                    command.Parameters.Add(param);
                this._parameters = new List<SqlParameter>();

                int result = 0;

                //try
                //{
                result = command.ExecuteNonQuery();
                //}
                //catch (SqlException ex)
                //{
                //   throw new QAException("4100", ex, ErrorMsgs.E4100_WHAT_HAPPENED_MESSAGE, 
                //}

                return result;
            }
        }

        /// <summary>
        /// Execute a non-select sql statement
        /// </summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <returns>Integer greater than 0 for success</returns>
        protected int ExecuteNonQuery(string commandText, CommandType commandType)
        {
            return ExecuteNonQuery(commandText, commandType, null);
        }

        /// <summary> Execute a scalar sql statement
        /// </summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <param name="parameters">Optional array of parameters</param>
        /// <returns>Object containing the scalar value</returns>
        protected object ExecuteScalar(string commandText, CommandType commandType, IDataParameter[] parameters)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(commandText, connection) { CommandType = commandType };

                if (parameters != null)
                {
                    foreach (IDataParameter parameter in parameters)
                        command.Parameters.Add(parameter);
                }
                foreach (SqlParameter param in this._parameters)
                    command.Parameters.Add(param);
                this._parameters = new List<SqlParameter>();

                object result = null;

                //try
                //{
                result = command.ExecuteScalar();
                //}
                //catch (SqlException ex)
                //{
                //   throw new QAException("4100", ex, ErrorMsgs.E4100_WHAT_HAPPENED_MESSAGE, 
                //}

                return result;
            }
        }

        /// <summary>
        /// Execute a scalar sql statement
        /// </summary>
        /// <param name="commandText">Sql to be executed</param>
        /// <param name="commandType">Text, stored procedure, etc.</param>
        /// <returns>Object containing the scalar value</returns>
        protected object ExecuteScalar(string commandText, CommandType commandType)
        {
            return ExecuteScalar(commandText, commandType, null);
        }

        // Unused methods
        //protected IDataParameter CreateParameter(string name, object value)
        //{
        //    return new SqlParameter(name, value ?? DBNull.Value);
        //}

        //protected IDataParameter CreateParameter(string name, string value)
        //{
        //    return new SqlParameter
        //               {
        //                   SqlDbType = SqlDbType.VarChar,
        //                   ParameterName = name,
        //                   Value = (object)value ?? DBNull.Value
        //               };
        //}

        //protected IDataParameter CreateOutParameter(string name, DbType type)
        //{
        //    return new SqlParameter
        //               {
        //                   ParameterName = name,
        //                   Direction = ParameterDirection.Output,
        //                   DbType = type
        //               };
        //}
    }
}
