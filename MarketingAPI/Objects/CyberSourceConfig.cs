﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class CyberSourceConfig
    {
        public string MerchantID { get; set; }

        public string SecretKey { get; set; }

        public string AccessKey { get; set; }

        public string ProfileID { get; set; }
    }
}