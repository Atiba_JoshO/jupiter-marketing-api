﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public class Driver
    {
        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String MiddleName { get; set; }

        public DateTime DOB { get; set; }

        public DriverGender Gender { get; set; }

        public IsMarried IsMarried { get; set; }

        public IsViolations IsViolations { get; set; }
    }
}