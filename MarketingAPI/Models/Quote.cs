﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MarketingAPI.Object;

namespace MarketingAPI.Models
{
    public class Quote
    {
        public Quote()
        {
            Vehicles = new List<Vehicle>();
            Drivers = new List<Driver>();
            Coverage = new Coverage();
        }

        public String PerMonth { get; set; }

        public String DownPayment { get; set; }

        public String OneTimePayment { get; set; }

        public Coverage Coverage { get; set; }
        public List<Vehicle> Vehicles { get; set; }

        public List<Driver> Drivers { get; set; }

        public String MainLocationZip { get; set; }

        public String MainLocationCityState { get; set; }

        public int ProducerID { get; set; }

        public IsCurrentlyInsured IsCurrentlyInsured { get; set; }

        public WhenIsCoverageNeeded WhenIsCoverageNeeded { get; set; }

        public CoverageLimits CoverageLimits { get; set; }

        public String EmailAddress { get; set; }

        public String PhoneNumber { get; set; }

        public String StreetAddress { get; set; }

        public String AptNumber { get; set; }

        public String ZipCode { get; set; }

        public String City { get; set; }

        public String State { get; set; }

        public DateTime EffectiveDate { get; set; }
    }
}