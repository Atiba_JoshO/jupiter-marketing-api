﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Models
{
    public class QuoteResults
    {
        public QuoteResults()
        {

        }

        public String PerMonth { get; set; }

        public String DownPayment { get; set; }

        public String OneTime { get; set; }

        public String PolicyId { get; set; }

        public String PolicyNumber { get; set; }
    }
}