﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MarketingAPI.Object;

namespace MarketingAPI.Models
{
    public class Coverage
    {
        public Coverage()
        {
            
        }

        public string RatingVersionID { get; set; }
        public List<string> SLEachAccident { get; set; }
        public List<string> BIEachPerson { get; set; }
        
        //public string BIEachAccident { get; set; }
        public List<TopCoverage> BIEachAccident { get; set; }
        public List<SubCoverage> PDEachAccident { get; set; }
        public List<string> MedicalEachPerson { get; set; }
        public List<string> UninsuredCSLEachAccident { get; set; }
        public List<string> UninsuredCSLDeductible { get; set; }
        public List<string> UninsuredBIEachPerson { get; set; }
        public List<TopCoverage> UninsuredBIEachAccident { get; set; }
        public List<string> UninsuredBIDeductible { get; set; }
        public List<SubCoverage> UninsuredPDEachAccident { get; set; }
        public List<string> UninsuredPDDeductible { get; set; }
        public List<string> UnderinsuredCSLEachAccident { get; set; }
        public List<string> UnderinsuredBIEachPerson { get; set; }
        public List<TopCoverage> UnderinsuredBIEachAccident { get; set; }
        public List<SubCoverage> UnderinsuredPDEachAccident { get; set; }
        public List<string> ComprehensiveDeductible { get; set; }
        public List<string> CollisionDeductible { get; set; }
        public List<string> OtherThanACV { get; set; }
        public List<string> TowingAndLabor { get; set; }
        public List<string> Loan { get; set; }
        public List<string> Rental { get; set; }
        public List<string> CustomEquipment { get; set; }
        public List<string> TransportationExpenses { get; set; }
        public List<string> AccidentalDeathEachPerson { get; set; }
        public List<string> WorkLossEachPerson { get; set; }
        public List<string> SubstituteBuyBack { get; set; }
        public List<string> PIPLimit { get; set; }
        public List<string> GlassCoverage { get; set; }
    }
}


