﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using MarketingAPI.Object;

namespace MarketingAPI.Models
{
    public class PaymentProcess
    {
        public PaymentProcess(){

        }

        public String CyberSourceEndPoint { get; set; }

        public JObject FormField { get; set; }

        public CyberSourceConfig CyberSource { get; set; }
    }
}