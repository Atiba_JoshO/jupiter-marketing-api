﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.Object;

namespace MarketingAPI.Models
{
    public class ZipResponse
    {
        public ZipResponse()
        {
            
        }

        public int Action { get; set; }

        public List<ZipResponseItem> Zips { get; set; }

        public Boolean Highway { get; set; }

        public int RatingVersionID { get; set; }

        public string RedirectURL { get; set; }
    }    
}
