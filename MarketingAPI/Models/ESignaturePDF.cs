﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp;
using System.Web.Mvc;

namespace MarketingAPI.Models
{
    public class ESignaturePDF
    {
        public ESignaturePDF()
        {
            //SignatureDateTime = DateFormat(NOW(), "MM/DD/YYYY") & ' ' & TimeFormat(Now(), "hh:mm tt");
        }

        public int PolicyID { get; set; }

        public string DocPos { get; set; }

        public String Initials { get; set; }

        public String Name { get; set; }

        public DateTime SignatureDateTime { get; set; }

        public String HtmlUrl { get; set; }

        public String PdfUrl { get; set; }
    }
}