﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MarketingAPI.Object;

namespace MarketingAPI.Models
{
    public class ESignature
    {
        public ESignature()
        {
            
        }

        public List<SecurityQuestion> SecurityQuestion1 { get; set; }

        public List<SecurityQuestion> SecurityQuestion2 { get; set; }

        public List<Driver> Drivers { get; set; }
    }
}