﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MarketingAPI.Object
{
    public enum Status
    {
        Rate = 1,
        SureHits,
        MediaAlpha,
        Redirect
    }

    public enum CoverageOptions
    {
        Available = 1,
        Policy,
        Vehicle,
        Optional

    }

    public enum DriverGender
    {
        [Description("M")]
        Male = 1,
        [Description("F")]
        Female 
    }

    public enum IsMarried
    {
        [Description("S")]
        No = 0,
        [Description("M")]
        Yes
    }

    public enum IsViolations
    {
        No = 0,
        Yes
    }

    public enum IsCurrentlyInsured
    {
        No = 0,
        Yes
    }

    public enum IsPaid
    {
        [Description("Leased/ Financed")]
        No = 0,
        [Description("Paid For")]
        Yes
    }

    public enum IsFullCoverage
    {
        [Description("Minimum Coverage")]
        No = 0,
        [Description("Full Coverage")]
        Yes
    }

    public enum WhenIsCoverageNeeded
    {        
        [Description("Immediately")]
        Immediately = 1,
        [Description("Within the next week")]
        WithinTheNextWeek ,
        [Description("Within the next month")]
        WithinTheNextMonth,
        [Description("3 months or more")]
        ThreeMonthsOrMore
    }

    public enum CoverageLimits
    {
        [Description("$25,000/50,000/$25,000")]
        State = 1,
        [Description("$50,000/$100,000/$25,000")]
        Enhanced,
        [Description("$100,000/$300,000/$100,000")]
        Premium   
    }
}