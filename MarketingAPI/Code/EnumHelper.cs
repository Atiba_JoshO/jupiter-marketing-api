﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

using System.ComponentModel;
using System.Reflection;

/// <summary>
/// Summary description for EnumHelper
/// </summary>
namespace MarketingAPI.Helper
{
    public class EnumUtility
    {
        public static string GetDescription<T>(T EnumValue)
        {
            FieldInfo fi = EnumValue.GetType().GetField(EnumValue.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : EnumValue.ToString();
        }

        //public static Hashtable Values(Type enumType)
        //{
        //    string[] enumNm = Enum.GetNames(enumType);
        //    int[] enumVal = (int[])Enum.GetValues(enumType);

        //    Hashtable ht = new Hashtable();
        //    for (int cnt = 0; cnt < enumVal.Length; cnt++)
        //    {
        //        ht.Add(enumVal.GetValue(cnt), enumNm[cnt]);
        //    }
        //    return ht;
        //}

        //public static List<KeyValuePair<string, string>> GetValuesAndDescription(Type enumType)
        //{

        //    List<KeyValuePair<string, string>> kvPairList = new List<KeyValuePair<string, string>>();


        //    Array Values = Enum.GetValues(enumType);
        //    foreach (int Value in Values)
        //    {
        //        var type = enumType.GetType();
        //        var memInfo = type.GetMember(enumType.ToString());
        //        var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
        //        var description = ((DescriptionAttribute)attributes[0]).Description;

        //        kvPairList.Add(new KeyValuePair<string, string>(description, ));
        //    }

        //    return kvPairList;
        //}

        public static Enum GetEnumValueFromDescription(Type MyType, string Description)
        {
            Enum retEnumValue = null;

            foreach (Enum e in Enum.GetValues(MyType))
            {
                string sValue = GetDescription((Enum)e);
                if (sValue.ToLower() == Description.ToLower())
                {
                    retEnumValue = e;
                    break;
                }
            }

            return retEnumValue;
        }

        public static string GetDescriptionFromValue(Enum value, Type type)
        {
            FieldInfo fi = type.GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }


        public static string GetDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }
    }
}