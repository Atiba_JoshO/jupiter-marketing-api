﻿using System;
using System.Data.SqlTypes;
using System.Xml;

namespace MarketingAPI.Object
{
    public static class NullConverter
    {
        public static bool IsNull(object value)
        {
            return (value == null || value == DBNull.Value);
        }

        public static DateTimeOffset NoNullDates(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return DateTimeOffset.MinValue;
            }
            return (DateTimeOffset)valueToConvert;
        }

        public static DateTime NoNullDateTimes(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return DateTime.MinValue;
            }
            return (DateTime)valueToConvert;
        }

        public static DateTime? NullDateTimes(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return null;
            }
            return (DateTime)valueToConvert;
        }

        public static decimal NoNullDecimals(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return 0m;
            }
            return Convert.ToDecimal(valueToConvert);
        }

        public static double NoNullDoubles(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return 0;
            }
            return Convert.ToDouble(valueToConvert);
        }

        public static int NoNullInts(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return 0;
            }
            return Convert.ToInt32(valueToConvert);
        }

        public static Guid NoNullGuids(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return Guid.Empty;
            }
            return new Guid(valueToConvert.ToString());
        }

        public static string NoNullStrings(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return string.Empty;
            }
            return valueToConvert.ToString();
        }

        public static char NoNullChars(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return char.MinValue;
            }
            return Convert.ToChar(valueToConvert);
        }

        public static bool NoNullBooleans(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return false;
            }
            return Convert.ToBoolean(valueToConvert);
        }

        public static TimeSpan NoNullTimes(object valueToConvert)
        {
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return DateTime.MinValue.TimeOfDay;
            }
            return DateTime.Parse(valueToConvert.ToString()).TimeOfDay;
        }

        /// <summary>
        /// Accepts a SqlXml field and returns a XmlDocument.
        /// </summary>
        /// <param name="valueToConvert">SqlXml field to be checked.</param>
        /// <returns>XmlDocument, empty or otherwise.</returns>
        public static XmlDocument NoNullXml(object valueToConvert)
        {
            XmlDocument result = new XmlDocument();
            if (valueToConvert == null || valueToConvert == DBNull.Value)
            {
                return result;
            }
            result.LoadXml(((SqlXml)valueToConvert).Value);
            return result;
        }
    }
}
