﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketingAPI.Object
{
    public enum ZipAction
    {
        Rate = 1,
        SureHits,
        MediaAlpha,
        Redirect
    }
}