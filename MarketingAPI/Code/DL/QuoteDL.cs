﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using MarketingAPI.Object;
using MarketingAPI.Models;
using MarketingAPI.Helper;

namespace MarketingAPI.DL
{
    public class QuoteDL : DataObject
    {
        public QuoteDL(string connectionString)
            : base(connectionString)
        {
        }

        public Boolean UpsertInsured(Quote quote, Boolean isInsert)
        {
            SetParameter("@fname1", quote.Drivers[0].FirstName);
            SetParameter("@middle1", quote.Drivers[0].MiddleName);
            SetParameter("@lname1", quote.Drivers[0].LastName);
            SetParameter("@address1", quote.StreetAddress);
            SetParameter("@address2", quote.AptNumber);
            SetParameter("@city", quote.City);
            SetParameter("@state", quote.State);
            SetParameter("@zip", quote.ZipCode);
            SetParameter("@county", "");
            SetParameter("@email", quote.EmailAddress);
            SetParameter("@phone", quote.PhoneNumber);
            SetParameter("@howSoon", EnumUtility.GetDescriptionFromValue(quote.WhenIsCoverageNeeded, typeof(WhenIsCoverageNeeded)));
            SetParameter("@PrefCoverageLimit", EnumUtility.GetDescriptionFromValue(quote.CoverageLimits, typeof(CoverageLimits)));
   

            int insuredID = 0;

            if (isInsert)
            {
                //insuredID = (int)ExecuteScalar("Service_InsertInsured", CommandType.StoredProcedure);
            }
            else
            {
                //insuredID = (int)ExecuteScalar("Service_InsertInsured", CommandType.StoredProcedure);
            }



            if (insuredID > 0)
                return true;

            return false;
        }

        public Boolean UpsertPolicy(Quote quote, Boolean isInsert)
        {
  

            //SetParameter("@policyNum", (input > 0) ? "positive" : "TBD");
            //SetParameter("@insuredID", (input > 0) ? "positive" : "1");
            //SetParameter("@companyID",(input > 0) ? "positive" : "1");
            //SetParameter("@effectiveDate", (input > 0) ? "positive" : String.Format("{0:yyyy-MM-dd}", DateTime.Now));
            //SetParameter("@producerID", (input > 0) ? "positive" : "1");
            //SetParameter("@policyTerm",(input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@policyType", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@stateID", (input > 0) ? "positive" : 1);
            //SetParameter("@status", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@paymentPlanID", (input > 0) ? "positive" : 1);
            //SetParameter("@depositPercent", (input > 0) ? "positive" : 100);
            //SetParameter("@installmentCount", (input > 0) ? "positive" : 0);
            //SetParameter("@imported", (input > 0) ? "positive" : 0);
            //SetParameter("@isDirect", (input > 0) ? "positive" : 0);
            //SetParameter("@policyProgram", (input > 0) ? "positive" : 0);


            //_clsSQL.addFilterValues("InsuredID", quote..ToString());

            //var sql = string.Empty;

            //if (isInsert)
            //{
            //    //sql = _clsSQL.getSQL(Riverport.SQL.sql.enumQueryTypes.insert, "insuredId");
            //}
            //else
            //{
            //    //sql = _clsSQL.getSQL(Riverport.SQL.sql.enumQueryTypes.update, "insuredId");
            //}

            //var result = _mydata.resultQuery(sql);

            //if (Convert.ToInt32(result) > 0)
            //    return true;

            return false;
        }

        public Boolean UpsertDriver(Quote quote, Boolean isInsert)
        {


            //SetParameter("@policyNum", (input > 0) ? "positive" : "TBD");
            //SetParameter("@insuredID", (input > 0) ? "positive" : "1");
            //SetParameter("@companyID", (input > 0) ? "positive" : "1");
            //SetParameter("@effectiveDate", (input > 0) ? "positive" : String.Format("{0:yyyy-MM-dd}", DateTime.Now));
            //SetParameter("@producerID", (input > 0) ? "positive" : "1");
            //SetParameter("@policyTerm", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@policyType", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@stateID", (input > 0) ? "positive" : 1);
            //SetParameter("@status", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@paymentPlanID", (input > 0) ? "positive" : 1);
            //SetParameter("@depositPercent", (input > 0) ? "positive" : 100);
            //SetParameter("@installmentCount", (input > 0) ? "positive" : 0);
            //SetParameter("@imported", (input > 0) ? "positive" : 0);
            //SetParameter("@isDirect", (input > 0) ? "positive" : 0);
            //SetParameter("@policyProgram", (input > 0) ? "positive" : 0);


           

            return false;
        }

        public Boolean UpsertVehicle(Quote quote, Boolean isInsert)
        {
           
            //SetParameter("@policyNum", (input > 0) ? "positive" : "TBD");
            //SetParameter("@insuredID", (input > 0) ? "positive" : "1");
            //SetParameter("@companyID", (input > 0) ? "positive" : "1");
            //SetParameter("@effectiveDate", (input > 0) ? "positive" : String.Format("{0:yyyy-MM-dd}", DateTime.Now));
            //SetParameter("@producerID", (input > 0) ? "positive" : "1");
            //SetParameter("@policyTerm", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@policyType", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@stateID", (input > 0) ? "positive" : 1);
            //SetParameter("@status", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@paymentPlanID", (input > 0) ? "positive" : 1);
            //SetParameter("@depositPercent", (input > 0) ? "positive" : 100);
            //SetParameter("@installmentCount", (input > 0) ? "positive" : 0);
            //SetParameter("@imported", (input > 0) ? "positive" : 0);
            //SetParameter("@isDirect", (input > 0) ? "positive" : 0);
            //SetParameter("@policyProgram", (input > 0) ? "positive" : 0);


            

            return false;
        }

        public Boolean UpsertAuto(Quote quote, Boolean isInsert)
        {
          

            //SetParameter("@policyNum", (input > 0) ? "positive" : "TBD");
            //SetParameter("@insuredID", (input > 0) ? "positive" : "1");
            //SetParameter("@companyID", (input > 0) ? "positive" : "1");
            //SetParameter("@effectiveDate", (input > 0) ? "positive" : String.Format("{0:yyyy-MM-dd}", DateTime.Now));
            //SetParameter("@producerID", (input > 0) ? "positive" : "1");
            //SetParameter("@policyTerm", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@policyType", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@stateID", (input > 0) ? "positive" : 1);
            //SetParameter("@status", (input > 0) ? "positive" : DBNull.Value);
            //SetParameter("@paymentPlanID", (input > 0) ? "positive" : 1);
            //SetParameter("@depositPercent", (input > 0) ? "positive" : 100);
            //SetParameter("@installmentCount", (input > 0) ? "positive" : 0);
            //SetParameter("@imported", (input > 0) ? "positive" : 0);
            //SetParameter("@isDirect", (input > 0) ? "positive" : 0);
            //SetParameter("@policyProgram", (input > 0) ? "positive" : 0);


           

            return false;
        }
    }
}