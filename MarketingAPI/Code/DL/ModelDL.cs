﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.Object;

namespace MarketingAPI.DL
{
    public class ModelDL : DataObject
    {
        public ModelDL(string connectionString) : base(connectionString)
        {

        }

        public List<string> GetModel(int modelYear, string makeName)
        {
            SetParameter("@modelYear", SqlDbType.Int, modelYear);
            SetParameter("@makeName", SqlDbType.NVarChar, makeName);
            IEnumerable<string> temp = ExecuteDataReaderToBasicList<string>("modelName", "GetModelInfo", CommandType.StoredProcedure);

            List<string> data = temp.ToList();

            return data;
        }
    }
}
