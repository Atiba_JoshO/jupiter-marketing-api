﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.Object;

namespace MarketingAPI.DL
{
    public class CoverageDL : DataObject
    {
        public CoverageDL(string connectionString) : base(connectionString)
        {
           
        }

        public List<String> GetCoverage(int ratingVersionID, string requireSpecialCoverages, string coverageType)
        {
            SetParameter("@ratingVersionID", SqlDbType.Int, ratingVersionID);
            SetParameter("@requireSpecialCoverages", SqlDbType.NVarChar, requireSpecialCoverages);
            SetParameter("@coverageType", SqlDbType.NVarChar, coverageType);

            IEnumerable<string> coverage = ExecuteDataReaderToBasicList<string>("amount", "Service_GetCoverage", CommandType.StoredProcedure);
            
            List<string> data = coverage.ToList();
  
            

            return data;
        }

        public int GetCoverageTypeID(int ratingVersionID, string description)
        {
            string sql = String.Format("SELECT coverageTypeID FROM CoverageType WHERE ratingVersionID = {0} AND description = '{1}'", ratingVersionID, description);

            var scalar =  ExecuteScalar(sql, CommandType.Text);

            return (int)Convert.ChangeType(scalar, typeof(int));
        }

        //public int GetStateID(int ratingVersionID)
        //{
        //    string sql = String.Format("SELECT stateID FROM RatingVersion WHERE ratingVersionID = {0}", ratingVersionID);

        //    var scalar = ExecuteScalar(sql, CommandType.Text);

        //    return (int)Convert.ChangeType(scalar, typeof(int));
        //}

        public List<TopCoverage> GetTopCoverage(int ratingVersionID, int coverageTypeID, string eachPerson)
        {
            string sql = String.Format(@"
                SELECT autoCoverageOptionID, perAccident                
                    FROM autoCoverageOption WITH (NOLOCK)
                        WHERE coverageTypeID = {0}
                            AND ratingVersionID = {1}                     
                                ORDER BY perAccident
                  ", coverageTypeID, ratingVersionID, eachPerson);
                   //AND perPerson = {2}
           
            IEnumerable<TopCoverage> topCoverage = ExecuteDataReaderToList<TopCoverage>(sql, CommandType.Text);

            List<TopCoverage> data = topCoverage.ToList();

            return data;
        }

        public List<SubCoverage> GetSubCoverage(int autoCoverageOptionID)
        {
           string sql = String.Format(@"
               SELECT perPerson, perAccident, defaultValue
		        FROM autoCoverageOption_Sub WITH (NOLOCK)
		        WHERE autoCoverageOptionID = {0}
			        AND coverageFlag <> 3		        
		        ORDER BY perAccident 
            ", autoCoverageOptionID);


           IEnumerable<SubCoverage> subCoverage = ExecuteDataReaderToList<SubCoverage>(sql, CommandType.Text);

           List<SubCoverage> data = subCoverage.ToList();

            return data;            
        }
    }
}
