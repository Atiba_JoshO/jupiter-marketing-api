﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.Object;
using MarketingAPI.Models;


namespace MarketingAPI.DL
{
    public class MakeDL : DataObject
    {
        public MakeDL(string connectionString) : base(connectionString)
        {

        }

        public List<string> GetMake(int modelYear)
        {
            SetParameter("@modelYear", SqlDbType.Int, modelYear);
            IEnumerable<string> make = ExecuteDataReaderToBasicList<string>("makeName", "GetMakeInfo", CommandType.StoredProcedure);

            List<string> data = make.ToList();

            return data;
        }
    }
}
