﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MarketingAPI.Models;
using Riverport.Core.Data;

using MarketingAPI.Object;

namespace MarketingAPI.DL
{
    public class EsignatureDL : Riverport.Core.Data.DaoBase
    {
        public List<SecurityQuestion> GetSecurityQuestion(string whichQuestion)
        {
            var sql = "SELECT securityQuestionID, securityQuestion FROM EsigFormData_SecurityQuestions WHERE Active = 1 AND securityQuestionNum = " + whichQuestion;
            return GetMultiple(sql, Map);
        }

        public SecurityQuestion Map(System.Data.DataRow dr)
        {
            if (dr == null) return null;
            var result = new SecurityQuestion();
            result.ID = DataRecordHelper.ConvertIntFromDb(dr["securityQuestionID"]);
            result.Question = DataRecordHelper.ConvertStringFromDb(dr["securityQuestion"]);            

            return result;
        } 
    }
}