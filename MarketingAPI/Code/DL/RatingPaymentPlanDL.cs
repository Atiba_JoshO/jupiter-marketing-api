﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using MarketingAPI.Object;
using MarketingAPI.Models;
using MarketingAPI.Helper;

namespace MarketingAPI.DL
{
    public class RatingDL : DataObject
    {
        public RatingDL(string connectionString) : base(connectionString)
        {
        }

        public RatingInfo GetRatingInfo(int producerId, int stateId, string policyType, DateTime effectiveDate)
        {
            string sql = String.Format(@"
                SELECT TOP 1 ratingVersionID, RV.companyID, policyTerm
                    FROM JupiterMGA_Rate..RatingVersion RV WITH (NOLOCK)
                        JOIN ProducerPolicyType PPT WITH (NOLOCK) ON PPT.companyID=RV.companyID 
        						                AND PPT.stateID=RV.stateID AND PPT.quoteStatus = 1			
						                        AND PPT.producerID = (SELECT mainProducerID
					            			                    FROM Producer 
								                                WHERE producerID = {0})
                    WHERE RV.stateID = {1}
		                AND RV.policyType = {2}
		                AND DATEDIFF(day, newBusinessDate, '{3}') >= 0
		                AND ((stopNewDate IS NULL) OR (DATEDIFF(day, stopNewDate, '{4}') <= 0))
	                ORDER BY newBusinessDate DESC
            ", producerId, stateId, policyType, effectiveDate, effectiveDate);


            RatingInfo ratingInfo = ExecuteDataReaderToObject<RatingInfo>(sql, CommandType.Text);

            return ratingInfo;   
        }

        public PaymentPlanInfo GetPaymentPlanInfo(int ratingVersionId, int stateId, string policyType, DateTime effectiveDate)
        {
            string sql = String.Format(@"
              SELECT TOP 1 paymentPlanID, installmentCharge, installmentCount, depositPercent
	            FROM PaymentPlan WITH (NOLOCK)
	            WHERE 
    	            ratingVersionID = {0}
    	            AND stateID = {1}
		            AND policyType = {2}
		            AND installmentCount = 5
		            AND DATEDIFF(day, startDate, '{3}') >= 0
		            AND ((stopDate IS NULL) OR (DATEDIFF(day, stopDate, '{3}') <= 0))

			
    	                    AND useNewBusiness = 1        
                
		            ORDER BY startDate DESC
            ", ratingVersionId, stateId, policyType, effectiveDate, effectiveDate);


            PaymentPlanInfo paymentPlanInfo = ExecuteDataReaderToObject<PaymentPlanInfo>(sql, CommandType.Text);

            return paymentPlanInfo;
        }  
    }
}