﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.Object;
using MarketingAPI.Models;

namespace MarketingAPI.DL
{
    public class StyleDL : DataObject
    {
        public StyleDL(string connectionString)
            : base(connectionString)
        {

        }

        public List<StyleResponseItem> GetStyle(int modelYear, string makeName, string modelName)
        {
            SetParameter("@modelYear", SqlDbType.Int, modelYear);
            SetParameter("@makeName", SqlDbType.NVarChar, makeName);
            SetParameter("@modelName", SqlDbType.NVarChar, modelName);
            IEnumerable<StyleResponseItem> temp = ExecuteDataReaderToList<StyleResponseItem>("GetStyleInfo", CommandType.StoredProcedure);

            List<StyleResponseItem> data = temp.ToList();

            return data;
        }

        public VehicleSymbol GetStyleSymbol(int modelYear, int styleId)
        {
            SetParameter("@year", SqlDbType.Int, modelYear);
            SetParameter("@styleID", SqlDbType.Int, styleId);
            VehicleSymbol symbols = ExecuteDataReaderToObject<VehicleSymbol>("GetStyleSymbol", CommandType.StoredProcedure);

            return symbols;
        }
    }
}
