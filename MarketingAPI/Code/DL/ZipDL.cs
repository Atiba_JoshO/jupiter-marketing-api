﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.Object;
using MarketingAPI.Models;

namespace MarketingAPI.DL
{
    public class ZipDL : DataObject
    {
        public ZipDL(string connectionString) : base(connectionString)
        {
        }

        public List<ZipResponseItem> CheckZip(string zip, int producerID)
        {
            SetParameter("@zip", SqlDbType.VarChar, zip);
            SetParameter("@producerID", SqlDbType.Int, producerID);
            IEnumerable<ZipResponseItem> temp = ExecuteDataReaderToList<ZipResponseItem>("Service_CheckZip", CommandType.StoredProcedure);

            List<ZipResponseItem> data = temp.ToList();

            return data;
        }

        public int GetRatingVersionID(int producerID)
        {
            int ratingVersionID;

            SetParameter("@producerID", SqlDbType.Int, producerID);
            var scalar = ExecuteScalar("Service_ZipRatingVersion", CommandType.StoredProcedure);

            if (scalar != null)
            {
                ratingVersionID = (int)Convert.ChangeType(scalar, typeof(int));
            }
            else
            {
                ratingVersionID = 0;
            }

            return ratingVersionID;
        }

        public string GetRedirectURL(int producerID, string zip)
        {
            string redirectURL;

            SetParameter("@zipCode", SqlDbType.Int, zip);
            SetParameter("@producerID", SqlDbType.Int, producerID);
            var scalar = ExecuteScalar("Service_ZipRedirect", CommandType.StoredProcedure);

            if (scalar != null)
            {
                redirectURL = (string)Convert.ChangeType(scalar, typeof(string));
            }
            else
            {
                redirectURL = String.Empty;
            }

            return redirectURL;
        }

        public Boolean IsHighway(int producerID)
        {
            Boolean isHighway;

            SetParameter("@producerID", SqlDbType.Int, producerID);
            var scalar = ExecuteScalar("Service_ZipHighway", CommandType.StoredProcedure);

            if (scalar != null)
            {
                isHighway = (Boolean)Convert.ChangeType(scalar, typeof(Boolean));
            }
            else
            {
                isHighway = false;
            }

            return isHighway;
        }
    }
}
