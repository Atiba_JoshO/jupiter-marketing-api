﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;

using Newtonsoft.Json.Linq;

namespace MarketingAPI.Helper
{
    public static class Utility
    {
       

        public static dynamic ParseJSON(string jsonContent)
        {
            dynamic data = JObject.Parse(jsonContent);
            //JObject data = JObject.Parse(jsonContent);
            //data.ToObject<List<string>>();
            return data;
        }

        public static string CalculateAge(DateTime birthday)
        {
            var yearsOld = DateTime.Today.Year - birthday.Year;
            if (DateTime.Today < birthday.AddYears(yearsOld)) yearsOld--;

            return yearsOld.ToString();
        }

        public static string ConnectionString(string db)
        {
             string connectionString = string.Empty;

            switch (db)
            {
                case "JupiterMGA_ChromeData":
                    connectionString = ConfigurationManager.ConnectionStrings["JupiterMGA_ChromeDataConnectionString"].ConnectionString;
                    break;
                case "JupiterMGA_Rate":
                    connectionString = ConfigurationManager.ConnectionStrings["JupiterMGA_RateConnectionString"].ConnectionString;
                    break;
                case "JupiterMGA":
                    connectionString = ConfigurationManager.ConnectionStrings["JupiterMGAConnectionString"].ConnectionString;
                    break;
            }

            return connectionString;
        }
    }
}