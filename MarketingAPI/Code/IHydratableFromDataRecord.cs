﻿using System;
using System.Data;

namespace MarketingAPI.Object
{
    public interface IHydratableFromDataRecord
    {
        void HydrateFromDataRecord(IDataRecord record);
    }
}
