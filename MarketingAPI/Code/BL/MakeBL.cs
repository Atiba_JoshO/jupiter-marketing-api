﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.DL;
using MarketingAPI.Object;
using MarketingAPI.Models;

namespace MarketingAPI.BL
{
    public class MakeBL
    {
        private MakeDL makeDL;
        private List<string> makes;
        private string connectionString;

        public MakeBL(string connectionString)
        {
            this.connectionString = connectionString;
            this.makeDL = new MakeDL(connectionString);
        }

        public List<string> GetMake(int modelYear)
        {
            this.makes = this.makeDL.GetMake(modelYear);
            return makes;
        }
    }
}
