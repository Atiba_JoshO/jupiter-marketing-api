﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.DL;
using MarketingAPI.Object;
using MarketingAPI.Models;

namespace MarketingAPI.BL
{
    public class ZipBL
    {
        private ZipDL zipDL;
        private List<ZipResponseItem> zips;
        private string connectionString;

        public ZipBL(string connectionString)
        {
            this.connectionString = connectionString;
            this.zipDL = new ZipDL(connectionString);
        }

        public List<ZipResponseItem> CheckZip(string zip, int producerID)
        {
            if (!String.IsNullOrEmpty(zip))
            {
                this.zips = this.zipDL.CheckZip(zip, producerID);
                return zips;
            }
            else
            {
                return null;
            }
        }

        //public ZipResponseItem CheckZip(string zip, int producerID)
        //{
        //    if (producerID > 0 && !String.IsNullOrEmpty(zip))
        //    {
        //        this.zipResponseitem = this.zipDL.CheckZip(zip, producerID);
        //        return zipResponseitem;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        public int GetRatingVersionID(int producerID)
        {
            if (producerID > 0)
            {
                return this.zipDL.GetRatingVersionID(producerID);
            }
            else
            {
                return 0;
            }
        }

        public string GetRedirectURL(int producerID, string zip)
        {
            if (producerID > 0 && !String.IsNullOrEmpty(zip))
            {
                return this.zipDL.GetRedirectURL(producerID, zip);
            }
            else
            {
                return String.Empty;
            }
        }

        public Boolean IsHighway(int producerID)
        {
            if (producerID > 0)
            {
                return this.zipDL.IsHighway(producerID);
            }
            else
            {
                return false;
            }
        }
    }
}
