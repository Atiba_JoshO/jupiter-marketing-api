﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.DL;
using MarketingAPI.Object;

namespace MarketingAPI.BL
{
    public class ModelBL
    {
        private ModelDL modelDL;
        private List<string> models;
        private string connectionString;

        public ModelBL(string connectionString)
        {
            this.connectionString = connectionString;
            this.modelDL = new ModelDL(connectionString);
        }

        public List<string> GetModel(int modelYear, string makeName)
        {
            this.models = this.modelDL.GetModel(modelYear, makeName);
            return models;
        }
    }
}
