﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.DL;
using MarketingAPI.Object;
using MarketingAPI.Models;

namespace MarketingAPI.BL
{
    public class PaymentBL
    {

        public String GetCyberSource(Boolean isLive)
        {
            if (isLive)
            {
                return "https://secureacceptance.cybersource.com/pay";
            }
            else
            {
                return "https://testsecureacceptance.cybersource.com/pay";
            }
        }

        public CyberSourceConfig GetConfig(string companyID)
        {
            CyberSourceConfig config = new CyberSourceConfig();

            switch (companyID)
            {
                case "12":
                    config.MerchantID = "313286637886";
                    config.SecretKey = "61c12532046a4c998174e10cd84dbd88897fe6f6a14547bb950cf2d473bbe579504dedcecfed426d978351ae89cabbe87df9bf25739149da8a1336a3f7c960e44315855e3bf14139af939370f4ca7d1c277fd08ea7be4c65a173f51588b454b4a45285ada3dd4d85bc8ff5914a2d2de05376adeaf8744a648c766c8692a1f5a9";
			        config.AccessKey = "92dc3fe2f4d13683bafc7e1c382ca711";
			        config.ProfileID = "Jup1200";
                    break;
                case "13":
                    config.MerchantID = "313292616882";
                    config.SecretKey = "cfa4ae0ed42048a49f2e3b72dfae4036c8cd390b3bdf424ba628ed99c861a66447f171dedd834903a2bdd49009013687b7d0a824f6224c7e8433199236576311184fa4fb674546b6b63572be5513e53edefd4761c0b14c85964551b3a72836ccc2ca91502f444464b8dcc9513c5a9cfafadedf2e3a0a4ff09143df738c12f6e3";
			        config.AccessKey = "2f118a2566923fb98ebdefe337bb4e3e";
			        config.ProfileID = "Jup1300";
                    break;
            }

            return config;
        }
    }
}