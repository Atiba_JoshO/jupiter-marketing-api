﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;

using MarketingAPI.DL;
using MarketingAPI.Object;

namespace MarketingAPI.BL
{
    public class EsignatureBL
    {
        public List<SecurityQuestion> GetSecurityQuestion(String whichQuestion)
        {
            var question = new EsignatureDL().GetSecurityQuestion(whichQuestion);
            if (question == null) return null;

 
            return question;
        }
    }
}