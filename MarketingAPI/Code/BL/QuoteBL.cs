﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MarketingAPI.DL;
using MarketingAPI.Object;
using MarketingAPI.Models;

namespace MarketingAPI.BL
{
    public class QuoteBL
    {
        private QuoteDL quoteDL;
        private string connectionString;

        public QuoteBL(string connectionString)
        {
            this.connectionString = connectionString;
            this.quoteDL = new QuoteDL(connectionString);
        }

        public Boolean UpsertInsured(Quote quote)
        {
            if (quote.Drivers[0].FirstName == null)
                return false;

            //look up
            Boolean isInsert = true;

            return this.quoteDL.UpsertInsured(quote, isInsert);
        }

        public Boolean UpsertPolicy(Quote quote)
        {
           //check data

            //upsert


            //update expiration date & ratingVersionID

            //select discounts surcharges sr22...

            //look up
            Boolean isInsert = true;

            return this.quoteDL.UpsertInsured(quote, isInsert);
        }

        public Boolean UpsertDriver(Quote quote)
        {
            //look up
            Boolean isInsert = true;

            return this.quoteDL.UpsertInsured(quote, isInsert);
        }

        public Boolean UpsertVehicle(Quote quote)
        {
            //look up
            Boolean isInsert = true;

            return this.quoteDL.UpsertInsured(quote, isInsert);
        }

        public Boolean UpsertAuto(Quote quote)
        {
            //look up
            Boolean isInsert = true;

            return this.quoteDL.UpsertInsured(quote, isInsert);
        }
    }
}