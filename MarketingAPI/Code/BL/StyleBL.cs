﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MarketingAPI.DL;
using MarketingAPI.Object;
using MarketingAPI.Models;

namespace MarketingAPI.BL
{
    public class StyleBL
    {
        private StyleDL styleDL;
        private List<StyleResponseItem> styles;
        private VehicleSymbol symbols;
        private string connectionString;

        public StyleBL(string connectionString)
        {
            this.connectionString = connectionString;
            this.styleDL = new StyleDL(connectionString);
        }

        public List<StyleResponseItem> GetStyles(int modelYear, string makeName, string modelName)
        {
            this.styles = this.styleDL.GetStyle(modelYear, makeName, modelName);
            return styles;
        }

        public VehicleSymbol GetStyleSymbol(int modelYear, int styleId)
        {
            this.symbols = this.styleDL.GetStyleSymbol(modelYear, styleId);
            return symbols;
        }
    }
}
