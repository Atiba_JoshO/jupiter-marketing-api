﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;

using MarketingAPI.DL;
using MarketingAPI.Object;

namespace MarketingAPI.BL
{
    public class RatingBL
    {
        private string connectionString;
        private RatingDL ratingDL;
        private RatingInfo ratingInfo;
        private PaymentPlanInfo paymentPlanInfo;

        public RatingBL(string connectionString, dynamic data)
        {
            this.connectionString = connectionString;
            this.ratingDL = new RatingDL(connectionString);
        }

        public RatingInfo GetRating(int producerId, int stateId, string policyType, DateTime effectiveDate)
        {
            this.ratingInfo = this.ratingDL.GetRatingInfo(producerId, stateId, policyType, effectiveDate);
            return ratingInfo;
        }

        public PaymentPlanInfo GetPayment(int ratingVersionId, int stateId, string policyType, DateTime effectiveDate)
        {
            this.paymentPlanInfo = this.ratingDL.GetPaymentPlanInfo(ratingVersionId, stateId, policyType, effectiveDate);
            return paymentPlanInfo;
        }
    }
}