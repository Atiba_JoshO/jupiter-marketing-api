﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;

using MarketingAPI.DL;
using MarketingAPI.Object;

namespace MarketingAPI.BL
{
    public class CoverageBL
    {
        private CoverageDL coverageDL;
        private List<String> coverages;
        private string connectionString;
        private List<CoverageValue> coverageValue;
        private List<CoveragesAvailable> coverageAvailable;
        private int ratingVersionID;

        public CoverageBL(string connectionString, int ratingVersionID)
        {
            this.connectionString = connectionString;
            this.coverageDL = new CoverageDL(connectionString);
            this.ratingVersionID = ratingVersionID;
            this.coverageValue = new List<CoverageValue>();
            this.coverageAvailable = new List<CoveragesAvailable>();
            GetXMLCoveragValue();
            GetXMLCoverageAvailable();            
        }

        public List<string> GetCoverage(string requireSpecialCoverages, string coverageType)
        {
            List<string> coverage = new List<string>();

            if (isCheckAvailablePolicy(coverageType))
            {
                coverage = GetCoverageValue(requireSpecialCoverages, coverageType);
            }

            if (isCheckVehicle(coverageType))
            {
                coverage = GetCoverageValue(requireSpecialCoverages, coverageType);
            }

            if (coverage != null) { 
                for (int i = 0; i < coverage.Count; i++)
                {
                    if (coverage[i].Contains("NO_COVx") || coverage[i].Contains("NO_COV"))
                        coverage[i] = "No Coverage";

                    coverage[i] = coverage[i].Replace("x", "");
                }
            }

            return coverage;
        }

        public Tuple<List<TopCoverage>, List<SubCoverage>> GetAssociatedCoverage(string coverageType, string coverageDesc, string eachPerson)
        {
            List<string> coverage = new List<string>();
            List<TopCoverage> topCoverage = new List<TopCoverage>();
            List<SubCoverage> subCoverage = new List<SubCoverage>();
            TopCoverage FirstCoverage = new TopCoverage();

            if (isCheckAvailablePolicy(coverageType))
            {
                int coverageTypeID = coverageDL.GetCoverageTypeID(this.ratingVersionID, coverageDesc);
            
                if (eachPerson == "NO_COVx" || eachPerson == "No Coverage")
                {
                    eachPerson = "0";
                }
                
                topCoverage = coverageDL.GetTopCoverage(ratingVersionID, coverageTypeID, eachPerson.Replace("x", ""));
                FirstCoverage = topCoverage.Last();
                //if (topCoverage.PerAccident == "0")
                //{
                //    topCoverage.PerAccident = "No Coverage";
                //}


                subCoverage = coverageDL.GetSubCoverage(FirstCoverage.AutoCoverageID);
            }

            return new Tuple<List<TopCoverage>, List<SubCoverage>>(topCoverage, subCoverage);

        }


        public List<string> GetCoverage(string requireSpecialCoverages, string coverageType, string coverageTypeCheckByPass)
        {
            List<string> coverage = new List<string>();

            if (isCheckAvailablePolicy(coverageTypeCheckByPass))
            {
                coverage = GetCoverageValue(requireSpecialCoverages, coverageType);
            }

            if(isCheckVehicle(coverageTypeCheckByPass))
            {
                coverage = GetCoverageValue(requireSpecialCoverages, coverageType);
            }

            return coverage;
        }

        public Boolean isCheckAvailablePolicy(string coverageType)
        {
            var item = this.coverageAvailable.FirstOrDefault(o => o.Abbr == coverageType);

            if (item == null)
                return false;

            if (item.Available && item.Policy)
                return true;

            return false;
        }

        public Boolean isCheckVehicle(string coverageType)
        {
            var item = this.coverageAvailable.FirstOrDefault(o => o.Abbr == coverageType);

            if (item == null)
                return false;

            if (item.Vehicle)
                return true;

            return false;
        }

        public List<string> GetCoverageValue(string requireSpecialCoverages, string coverageType)
        {
            var item = this.coverageValue.FirstOrDefault(o => o.Abbr == coverageType);
            if (item != null)
            {
                if (item.Value == "query")
                {
                    this.coverages = this.coverageDL.GetCoverage(ratingVersionID, requireSpecialCoverages, coverageType);
                }
                else
                {
                    string temp = item.Value;
                    this.coverages = this.coverages = temp.Split(',').ToList();
                }
            }
            else
            {
                this.coverages = null;
            } 
            
            return coverages;
        }
        
        public void GetXMLCoverageAvailable()
        {
            XDocument doc = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("~/Data/CoverageAvailable.xml"));

            IEnumerable<XElement> de =
              from item in doc.Descendants("ratingVersion")
              where item.Element("id").Value.Contains(this.ratingVersionID.ToString())
              select item.Element("coverages");

            foreach (var e in de.Elements())
            {
                List<string> options = e.Value.Split(',').ToList();
                CoveragesAvailable c = new CoveragesAvailable();

                c.Abbr = e.Attribute("abbr").Value;
                c.Available = Convert.ToBoolean(Convert.ToInt32(options[0]));
                c.Policy = Convert.ToBoolean(Convert.ToInt32(options[1]));
                c.Vehicle = Convert.ToBoolean(Convert.ToInt32(options[2]));
                c.Optional = Convert.ToBoolean(Convert.ToInt32(options[3]));
                this.coverageAvailable.Add(c);
            }
                



                //List<string> options = e.Value.Split(',').ToList();
                //CoveragesAvailable c = new CoveragesAvailable();

                //if (e.Attribute("override").Value == "override")
                //{
                //    switch (e.Attribute("operator").Value)
                //    {
                //        case "eq":
                //            if (this.ratingVersionID == Convert.ToInt32(e.Attribute("ratingversion").Value))
                //                c = GetXMLCoverageAvailableHelper(options, e.Attribute("abbr").Value);
                //            break;

                //        case "gte":
                //            if (this.ratingVersionID >= Convert.ToInt32(e.Attribute("ratingversion").Value))
                //                c = GetXMLCoverageAvailableHelper(options, e.Attribute("abbr").Value);
                //            break;

                //        case "gt":
                //            if (this.ratingVersionID > Convert.ToInt32(e.Attribute("ratingversion").Value))
                //                c = GetXMLCoverageAvailableHelper(options, e.Attribute("abbr").Value);
                //            break;

                //        case "lte":
                //            if (this.ratingVersionID <= Convert.ToInt32(e.Attribute("ratingversion").Value))
                //                c = GetXMLCoverageAvailableHelper(options, e.Attribute("abbr").Value);
                //            break;

                //        case "lt":
                //            if (this.ratingVersionID < Convert.ToInt32(e.Attribute("ratingversion").Value))
                //                c = GetXMLCoverageAvailableHelper(options, e.Attribute("abbr").Value);
                //            break;
                //   }
                //}
                //else
                //{
                //    c = GetXMLCoverageAvailableHelper(options, e.Attribute("abbr").Value);
                    
                //}               
        }

        private CoveragesAvailable GetXMLCoverageAvailableHelper(List<string> options, string abbr)
        {
            CoveragesAvailable c = new CoveragesAvailable();

            c.Abbr = abbr;
            c.Available = Convert.ToBoolean(Convert.ToInt32(options[0]));
            c.Policy = Convert.ToBoolean(Convert.ToInt32(options[1]));
            c.Vehicle = Convert.ToBoolean(Convert.ToInt32(options[2]));
            c.Optional = Convert.ToBoolean(Convert.ToInt32(options[3]));

            return c;
        }
                
        public void GetXMLCoveragValue()
        {
            XDocument doc = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("~/Data/CoverageValue.xml"));

            IEnumerable<XElement> de =
              from item in doc.Descendants("ratingVersion")
              where item.Element("id").Value.Contains(this.ratingVersionID.ToString())
              select item.Element("coverages");

            foreach (var e in de.Elements())
            {
                CoverageValue c = new CoverageValue();
                c.Name = e.Attribute("name").Value;
                c.Value = e.Value;
                c.Abbr = e.Attribute("abbr").Value;
                this.coverageValue.Add(c);
            }
        }
    }
}
